PREFIX      := arm-wrs-linux-gnueabi-armv6jel_vfp-uclibc_small-
APP			:= test
all:		$(APP)

CC			:= $(PREFIX)gcc
CXX			:= $(PREFIX)g++
AR			:= $(PREFIX)ar
LD			:= $(PREFIX)ld

CFLAGS		+= -Wall -W -O2 -g

LD_FLAGS	+= -L/opt/workdir/SQLite3/target.arm/lib -lrt -lsqlite3 -ldl -lpthread

INC_FLAG    += -I$(APP_ROOT)/sqlite3wrapper -I/opt/workdir/SQLite3/target.arm/include

APP_ROOT	:= $(shell pwd)

APP_LIB		:= sqlite3wrapper

export CC CXX AR LD CFLAGS INC_FLAG LD_FLAGS APP_ROOT

CSRCS		:= main.c 
CXXSRCS		:= parameters.cpp

CXXOBJS		:= $(CXXSRCS:.cpp=.o)
COBJS		:= $(CSRCS:.c=.o)
OBJS		:= $(CXXOBJS) $(COBJS)

DEPS		:= $(OBJS:.o=.d)

MODDEP      := moddependensxxx.d

$(MODDEP):
	@printf ".PHONY : $(APP_LIB) \n\n" > $@
	@printf "$(APP_LIB) :\n\t$(MAKE) -C $(APP_LIB) TARGET=$(APP_LIB).a\n\n" >> $@

%.o : %.c
	@echo "Compile and Assemble---------- [$<]"
	@$(CC) $(CFLAGS) $(INC_FLAG) -c $< -o $@
%.d : %.c
	@set -e;$(CC) $(CFLAGS) $(INC_FLAG) $(CPPFLAGS) -MM $< | sed -e 's/$(basename $@).o/$(basename $@).o $(basename $@).d/' > $@
%.o : %.cpp
	@echo "Compile and Assemble---------- [$<]"
	@$(CXX) $(CFLAGS) $(INC_FLAG) -c $< -o $@
%.d : %.cpp
	@set -e;$(CXX) $(CFLAGS) $(INC_FLAG) $(CPPFLAGS) -MM $< | sed -e 's/$(basename $@).o/$(basename $@).o $(basename $@).d/' > $@

.PHONY:	$(APP) $(APP_LIB)

$(APP) : $(OBJS) $(APP_LIB)
	@echo "Link: $(OBJS) $(APP_LIB).a ---> $@"
	@$(CXX) $(OBJS) $(APP_LIB)/$(APP_LIB).a  -o $@ $(LD_FLAGS)

clean:
	$(RM) -f $(OBJS) $(DEPS) $(APP) $(MODDEP)

distclean : clean
	$(MAKE) -C $(APP_LIB)  TARGET=$(APP_LIB).a clean || exit 1; \

sinclude $(DEPS)
sinclude $(MODDEP)


##Create Static Library
#define BUILD_LIBRARY
#$(if $(wildcard $@),@$(RM) $@)
#$(if $(wildcard ar.mac),@$(RM) ar.mac)
#$(if $(filter %.a, $^),
#        @echo CREATE $@ > ar.mac
#        @echo SAVE >> ar.mac
#        @echo END >> ar.mac
#        @$(AR) -M < ar.mac
# )
#$(if $(filter %.o,$^),@$(AR) -q $@ $(filter %.o,$^))
#$(if $(filter %.a, $^),
#        @echo OPEN $@ > ar.mac
#        $(foreach LIB, $(filter %.a, %^),
#            @echo ADDLIB $(LIB) >> ar.mac)
#        @echo SAVE >> ar.mac
#        @echo END >> ar.mac
#        @$(AR) -M < ar.mac
#        @$(RM) ar.mac
# )
#endef
#
#$(TargetDir)/$(TargetFileName):$(OBJS)
#                                 $(BUILD_LIBRARY)
