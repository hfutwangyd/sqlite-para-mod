/** 
 * @FileName:   parameters.h
 * @Brief:      config module interface
 * @Author:     Retton
 * @Version:    V1.0
 * @Date:       2013-05-21
 * Copyright:   2012-2038 Anhui CHAOYUAN Info Technology Co.Ltd
 */
#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "define.h"

#define CFG_VERSION (20130529)

#ifdef __cplusplus
extern "C" 
{
#endif
    
    extern int parameterInit(const char *DbFilePath);
    extern int parameterBackup(const char *BackupDbFilePath);
    extern int parameterDeinit();
    
    extern int getParaDeviceinfo(DEVICE_INFO_T *pDeviceInfo);
    extern int setParaDeviceinfo(DEVICE_INFO_T *pDeviceInfo);
    extern int getParaRedlightzone(REDLIGHT_ZONE_T *pRedlightZone, int length);
    extern int setParaRedlightzone(REDLIGHT_ZONE_T *pRedlightZone, int length);

    extern int setParaVideoin(VIDEOIN *pVideoIn, int length);
    extern int getParaVideoin(VIDEOIN *pVideoIn, int length);
    extern int getParaVideoinLBL(VIDEOIN *pVideoIn, int length);

#ifdef __cplusplus
}
#endif

#endif
