/** 
 * @FileName:   define.h
 * @Brief:      parameter structure define 
 * @Author:     Retton
 * @Version:    V1.0
 * @Date:       2013-05-28
 * Copyright:   2012-2038 Anhui CHAOYUAN Info Technology Co.Ltd
 */
#ifndef _CY_DEFINE_H_
#define _CY_DEFINE_H_

#define MAX_VIDEO_IN    8
#define MAX_LENGTH      128
#define MIN_LENGTH      32
#define NAME_LEN        32
#define MAX_DAYS        8

typedef unsigned char       UINT8;
typedef unsigned short      UINT16;;
typedef unsigned int        UINT32;
typedef unsigned long long  UINT64;
typedef signed char         INT8;
typedef signed short        INT16;
typedef signed int          INT32;
typedef signed long long    INT64;

typedef struct
{
	UINT32 	handleType;							/*异常处理,异常处理方式的"或"结果*/
	UINT32	alarmOutTriggered;						/* 被触发的报警输出(此处限制最大报警输出数为32) */
}EXCEPTION_T;
typedef struct {	/* 4 bytes */
	UINT16	x;							/* 横坐标 */
	UINT16	y;							/* 纵坐标 */
} COORDINATE;
typedef struct {	/* 4 bytes */
	UINT16	width;						/* 宽度 */
	UINT16	height;						/* 高度 */
} SIZE;
typedef struct {	/* 8 bytes */
	COORDINATE	upLeft;					/* 左上角坐标 */
	SIZE		size;					/* 大小(宽度和高度) */
} ZONE;
typedef struct {	/* 8 bytes */
	COORDINATE	upLeft;					/* 左上角坐标 */
	UINT8		osdType;				/* OSD类型(主要是年月日格式) */
	UINT8		osdAttrib;				/* OSD属性: 透明，闪烁 */
	UINT8		dispWeek;				/* 是否显示星期 */
	UINT8		enable_date;			/* 是否显示日期 */
} OSDPARA;
typedef struct {	/* 12 bytes */
	UINT16 	streamAttr;					/* 码流属性(见定义) */
	UINT16 	intervalFrameI;					/* I帧间隔 */
	UINT32 	maxVideoBitrate;				/* 视频码率上限(单位：bps) */
	UINT32 	videoFrameRate;					/* 视频帧率，PAL：1/16-25；NTCS：1/16-30 */
	UINT8	BFrameNum;					/* B帧个数: 0:BBP, 1:BP, 2:P */
	UINT16	quality;					/* 图象质量 */
} COMPRESSION;
typedef struct {
	UINT32		motionLine[18];		            ///<18行22列方阵，每个方块表示16*16象素,每个长整型数据的0-21位分别对应每行中从左到右的22个块
	UINT8		motionLevel;		            ///<动态检测灵敏度，取0xff时关闭;(6最低;0最高) -> (0最低;5最高) 
	UINT8		motion_display;		            ///<移动侦测区域是否高亮显示，0-不显示，1-显示
	UINT8		res[2];
} MOTDETECT;
typedef struct {
	UINT16		bAllDayRecord;				    /* 是否全天录像 */
	UINT8		recType;				        /* 录象类型 */
}RECORDDAY;

typedef struct {
	UINT8			channelName[NAME_LEN];		/* 通道名称 */
	UINT8			videoStandard;				/* 视频制式 */
	UINT8			brightness;					/* 亮度   (0-255) */
	UINT8			contrast;					/* 对比度 (0-255) */
	UINT8			tonality;					/* 色调   (0-255) */
	UINT8			saturation;					/* 饱和度 (0-255) */
	UINT8			enableNameOsd;				/* 是否显示通道名称 */
	UINT8			logoAttrib;					/* LOGO属性:是否显示，透明，闪烁 */
	UINT8			enableHide;					/* 是否启动遮挡 */
	UINT8			enableSignalLostAlarm;		/* 信号丢失报警 */
	UINT8			enableMotDetAlarm;			/* 移动侦测报警 */
	UINT8			enableMaskAlarm;			/* 遮挡报警 0:关闭/1:灵敏度低/2:灵敏度中/3:灵敏度高 */
	UINT8			enableRecord;				/* 是否录象 */
	
	EXCEPTION_T		signalLostAlarmHandleType;	/* 信号丢失处理 */
	EXCEPTION_T		motDetAlarmHandleType;		/* 移动侦测处理 */
	EXCEPTION_T		maskAlarmHandleType;		/* 遮挡处理 */

	COORDINATE      nameOsdUpLeft;				/* 通道名称显示位置 */
	COORDINATE		logoUpLeft;					/* LOGO位置 */
	ZONE			hideArea;					/* 遮挡区域 */
	OSDPARA			osdPara;					/* OSD参数 */
	COMPRESSION		compressionPara;			/* 录象码流压缩参数 */
	COMPRESSION		netCompPara;				/* 网络传送码流压缩参数 */
	RECORDDAY		recordDay[MAX_DAYS];
} VIDEOIN;

typedef struct {

    char            DeviceNumber[MIN_LENGTH];
    char            DeviceSite[MAX_LENGTH];
    char            DirectionText[MAX_LENGTH];
    char            RoadCode[MIN_LENGTH];
    char            DepartmentCode[MIN_LENGTH];
    char            Latitude[MIN_LENGTH];
    char            Longtitude[MIN_LENGTH];
    unsigned int    DeviceMode;
    unsigned int    DirectionCode;
    unsigned int    Lane;
    unsigned int    SnapMode;

} DEVICE_INFO_T;

typedef struct {

    unsigned int    DefaultGain;
    unsigned int    MinGain;
    unsigned int    MaxGain;
    unsigned int    DefaultExposure;
    unsigned int    MinExposure;
    unsigned int    MaxExposure;
    unsigned int    VideoTargetGray;
    unsigned int    TriggerTargetGray;
    unsigned int    RGain;
    unsigned int    BGain;
    unsigned int    AEZone;
    unsigned int    AEWMode;
    unsigned int    DaytimeStartMin;
    unsigned int    DaytimeStartHour;
    unsigned int    DaytimeEndMin;
    unsigned int    DaytimeEndHour;
    unsigned int    QValue;
    unsigned int    AdSampleBits;
    unsigned int    SharpenFactor;

} AEW_PARA_T;

typedef struct {
    int    Index;
    int    Top;
    int    Left;
    int    Height;
    int    Width;
} REDLIGHT_ZONE_T;

#endif
