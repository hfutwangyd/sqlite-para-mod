/** 
 * @FileName:   parameters.cpp
 * @Brief:      
 * @Author:     Retton
 * @Version:    V1.0
 * @Date:       2013-05-21
 * Copyright:   2012-2038 Anhui CHAOYUAN Info Technology Co.Ltd
 */
#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <assert.h>
#include <string>
#include "parameters.h"
#include "sqlite3wrapper/sqlitewrapper.h"
#include "define.h"
#include "common.h"

#define FAILED  -1
#define SUCCESS 0

#define MAX_FIELD 64

#define ARRAY_SIZE(a)   (sizeof(a)/sizeof(a[0]))

static SQLite3DB Db;

static const char *TableList[] = {

    "Version",
    "DeviceInfo",
    "RedlightZone",
    "VideoInBase",
    "signalLostAlarmHandleType",
    "motDetAlarmHandleType",
    "maskAlarmHandleType",
    "nameOsdUpLeft",
    "logoUpLeft",
    "hideArea",
    "osdPara",
    "compressionPara",
    "netCompPara",
    "recordDay",
};

static const char *FieldList[][MAX_FIELD] = {

    {
        "Version",
    },//Version

    {
        "DeviceNumber",
        "DeviceSite",
        "DirectionText",
        "RoadCode",
        "DepartmentCode",
        "Latitude",
        "Longtitude",
        "DeviceMode",
        "DirectionCode",
        "Lane",
        "SnapMode",
    },//DeviceInfo

    {
        "Idx",
        "Top",
        "Left",
        "Height",
        "Width",
    },//redlightzone

    {
        "Idx",
        "channelName",
        "videoStandard",
        "brightness",
        "contrast",
        "tonality",
        "saturation",
        "enableNameOsd",
        "logoAttrib",
        "enableHide",
        "enableSignalLostAlarm",
        "enableMotDetAlarm",
        "enableMaskAlarm",
        "enableRecord",
    }, //VideoInBase

    {
        "Idx",
        "handleType",
        "alarmOutTriggered",
    },//signalLostAlarmHandleType

    {
        "Idx",
        "handleType",
        "alarmOutTriggered",
    },//motDetAlarmHandleType

    {
        "Idx",
        "handleType",
        "alarmOutTriggered",
    },//maskAlarmHandleType

    {
        "Idx",
        "x",
        "y",
    },//nameOsdUpLeft

    {
        "Idx",
        "x",
        "y",
    },//logoUpLeft

    {
        "Idx",
        "x",
        "y",
        "Height",
        "Width",
    },//hideArea

    {
        "Idx",
        "x",
        "y",
        "osdType",
        "osdAttrib",
        "dispWeek",
        "enable_date",
    },//osdPara

    {
        "Idx",
        "streamAttr",
        "intervalFrameI",
        "maxVideoBitrate",
        "videoFrameRate",
        "BFrameNum",
        "quality",
    },//compressionPara

    {
        "Idx",
        "streamAttr",
        "intervalFrameI",
        "maxVideoBitrate",
        "videoFrameRate",
        "BFrameNum",
        "quality",
    },//netCompPara

    {
        "Idx",
        "Idex",
        "bAllDayRecord",
        "recType",
    },//recordDay
};

static const char *CreateTableSql[] =
{
    //version table 
    "CREATE TABLE IF NOT EXISTS Version(\
        Version         INTEGER PRIMARY KEY)",

    "CREATE TABLE IF NOT EXISTS DeviceInfo(\
        DeviceNumber    TEXT    PRIMARY KEY,\
        DeviceSite      TEXT,\
        DirectionText   TEXT,\
        RoadCode        TEXT,\
        DepartmentCode  TEXT,\
        Latitude        TEXT,\
        Longtitude      TEXT,\
        DeviceMode      INTEGER,\
        DirectionCode   INTEGER,\
        Lane            INTEGER,\
        SnapMode        INTEGER)",

    "CREATE TABLE IF NOT EXISTS RedlightZone(\
        Idx             INTEGER PRIMARY KEY,\
        Top             INTEGER,\
        Left            INTEGER,\
        Height          INTEGER,\
        Width           INTEGER)",

    "CREATE TABLE IF NOT EXISTS VideoInBase(\
        Idx                     INTEGER PRIMARY KEY,\
        channelName             TEXT,\
        videoStandard           INTEGER,\
        brightness              INTEGER,\
        contrast                INTEGER,\
        tonality                INTEGER,\
        saturation              INTEGER,\
        enableNameOsd           INTEGER,\
        logoAttrib              INTEGER,\
        enableHide              INTEGER,\
        enableSignalLostAlarm   INTEGER,\
        enableMotDetAlarm       INTEGER,\
        enableMaskAlarm         INTEGER,\
        enableRecord            INTEGER)",

    "CREATE TABLE IF NOT EXISTS signalLostAlarmHandleType(\
        Idx                     INTEGER PRIMARY KEY,\
        handleType              INTEGER,\
        alarmOutTriggered       INTEGER)",

    "CREATE TABLE IF NOT EXISTS motDetAlarmHandleType(\
        Idx                     INTEGER PRIMARY KEY,\
        handleType              INTEGER,\
        alarmOutTriggered       INTEGER)",

    "CREATE TABLE IF NOT EXISTS maskAlarmHandleType(\
        Idx                     INTEGER PRIMARY KEY,\
        handleType              INTEGER,\
        alarmOutTriggered       INTEGER)",

    "CREATE TABLE IF NOT EXISTS nameOsdUpLeft(\
        Idx                     INTEGER PRIMARY KEY,\
        x                       INTEGER,\
        y                       INTEGER)",

    "CREATE TABLE IF NOT EXISTS logoUpLeft(\
        Idx                     INTEGER PRIMARY KEY,\
        x                       INTEGER,\
        y                       INTEGER)",

    "CREATE TABLE IF NOT EXISTS hideArea(\
        Idx                     INTEGER PRIMARY KEY,\
        x                       INTEGER,\
        y                       INTEGER,\
        Height                  INTEGER,\
        Width                   INTEGER)",

    "CREATE TABLE IF NOT EXISTS osdPara(\
        Idx                     INTEGER PRIMARY KEY,\
        x                       INTEGER,\
        y                       INTEGER,\
        osdType                 INTEGER,\
        osdAttrib               INTEGER,\
        dispWeek                INTEGER,\
        enable_date             INTEGER)",

    "CREATE TABLE IF NOT EXISTS compressionPara(\
        Idx                     INTEGER PRIMARY KEY,\
        streamAttr              INTEGER,\
        intervalFrameI          INTEGER,\
        maxVideoBitrate         INTEGER,\
        videoFrameRate          INTEGER,\
        BFrameNum               INTEGER,\
        quality                 INTEGER)",
        
    "CREATE TABLE IF NOT EXISTS netCompPara(\
        Idx                     INTEGER PRIMARY KEY,\
        streamAttr              INTEGER,\
        intervalFrameI          INTEGER,\
        maxVideoBitrate         INTEGER,\
        videoFrameRate          INTEGER,\
        BFrameNum               INTEGER,\
        quality                 INTEGER)",

    "CREATE TABLE IF NOT EXISTS recordDay(\
        Idx                     INTEGER,\
        Idex                    INTEGER,\
        bAllDayRecord           INTEGER,\
        recType                 INTEGER,\
        CONSTRAINT unionkey UNIQUE(Idx, Idex))",

};

static int versionMigrate(const char *TableName, const char *FieldArray[], int appFieldNum)
{
    try
    {
        SQLite3Buffer Buff;

        SQLite3Query Query = Db.ExecQuery(Buff.format("SELECT * FROM %q", TableName));

        int dbFieldNum = Query.GetFieldsNum();

        if (appFieldNum == dbFieldNum)
        {
            return SUCCESS;
        }
        else if (appFieldNum > dbFieldNum)
        {
            Db.BeginTransaction();
            for (int i = 0; i < appFieldNum; ++i)
            {
                bool inTable = false;
                for (int j = 0; j < dbFieldNum; ++j)
                {
                    if (0 == strcmp(FieldArray[i], Query.GetFieldName(j)))
                    {
                        inTable = true;
                    }
                }
                
                if (!inTable)
                {
                    Db.ExecSQL(Buff.format("ALTER TABLE %q ADD COLUMN %q DEFAULT 0",TableName,  FieldArray[i]));
                }
            }
            Db.CommitTransaction();
            return SUCCESS;
        }
        else
        {
            Db.BeginTransaction();
            std::string Sql = "CREATE TABLE temp_table AS SELECT ";
            for (int i = 0; i < appFieldNum-1; ++i)
            {
                Sql += FieldArray[i];
                Sql += ",";
            }
            Sql += FieldArray[appFieldNum-1];
            Sql += " ";
            Sql += "From %q";

            Db.ExecSQL(Buff.format(Sql.c_str(), TableName));
            Db.ExecSQL(Buff.format("DROP TABLE %q", TableName));
            Db.ExecSQL(Buff.format("ALTER TABLE temp_table RENAME To %q", TableName));
            Db.CommitTransaction();
            return SUCCESS;
        }
    }
    catch (SQLite3Exception &e)
    {
        if(Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

static int setVersion2Database(long int version)
{
    try
    {
        SQLite3Buffer Buff;

        int ret = Db.ExecScalar("SELECT COUNT(*) FROM Version");

        if (1 == ret)
        {
            Db.ExecSQL(Buff.format("UPDATE Version SET Version=%d", version));

            return SUCCESS;
        }
        else if (0 == ret)
        {
            Db.ExecSQL(Buff.format("INSERT INTO Version VALUES(%d)", version));

            return SUCCESS;
        }
        else
        {
            throw ret;
        }
    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
    catch (int ret)
    {
        LOG("Version Row Is Not Valid:%d", ret);
        return FAILED;
    }

}

static long int getVersionFromDB(void)
{
    try
    {
        SQLite3Table Table = Db.GetTable("SELECT * FROM Version");

        int nRow    = Table.GetRowsNum();
        int nField  = Table.GetFieldsNum();

        if (1 != nRow && 1 != nField)
        {
            throw 1;
        }

        return atol(Table.GetFieldValue("Version"));

    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
    catch (...)
    {
        LOG("Version Table Row or Field not 1");
        return FAILED;
    }
}

int parameterInit(const char *DbFilePath)
{
    try
    {
        if (NULL == DbFilePath)
        {
            LOG("DbFilePath is NULL");
            return FAILED;
        }

        bool bCreate = false;
        std::fstream fin(DbFilePath);
        if (!fin)
        {
            bCreate = true;
        }
        fin.close();

        Db.OpenDb(DbFilePath);
        
        if (bCreate)
        {
            Db.ExecSQL("PRAGMA PAGE_SIZE=4096");
            Db.ExecSQL("PRAGMA AUTO_VACUUM=1");
            Db.ExecSQL("PRAGMA SYNCHRONOUS=FULL");
        }

        unsigned int i = 0;

        for (i = 0; i < ARRAY_SIZE(CreateTableSql); ++i)
        {
            Db.ExecSQL(CreateTableSql[i]);
        }

        long int version = getVersionFromDB();
        if (FAILED == version)
        {
            LOG("Get Version Failed");
            return setVersion2Database(CFG_VERSION);
        }
        else
        {
            if (version == CFG_VERSION)
            {
                return SUCCESS;
            }
            else 
            {
                LOG("Version un accordance");
                for (unsigned int i = 0; i < ARRAY_SIZE(TableList); ++i)
                {
                    int appFieldNum = 0;
                    for (unsigned j = 0; ; ++j)
                    {
                        if (NULL == FieldList[i][j])
                        {
                            break;
                        }
                        appFieldNum += 1;
                    }
                    if (-1 == versionMigrate(TableList[i], FieldList[i], appFieldNum))
                    {
                        return FAILED;
                    }
                }

                return setVersion2Database(CFG_VERSION);
            }
        }
    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }

}

int parameterBackup(const char *BackupDbFilePath)
{
    try
    {
        Db.Backup(BackupDbFilePath);
        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int parameterDeinit()
{
    Db.CloseDb();
    return SUCCESS;
}

int getParaDeviceinfo(DEVICE_INFO_T *pDeviceInfo)
{
    assert(pDeviceInfo != NULL);
    try
    {
        SQLite3Table Table = Db.GetTable("SELECT * FROM DeviceInfo");

        int nRow = Table.GetRowsNum();

        if (nRow == 1)
        {
            Table.SetRow(nRow-1);//attention
            strcpy(pDeviceInfo->DeviceNumber,   Table.GetFieldValue("DeviceNumber"));
            strcpy(pDeviceInfo->DeviceSite,     Table.GetFieldValue("DeviceSite"));
            strcpy(pDeviceInfo->DirectionText,  Table.GetFieldValue("DirectionText"));
            strcpy(pDeviceInfo->RoadCode,       Table.GetFieldValue("RoadCode"));
            strcpy(pDeviceInfo->DepartmentCode, Table.GetFieldValue("DepartmentCode"));
            strcpy(pDeviceInfo->Latitude,       Table.GetFieldValue("Latitude"));
            strcpy(pDeviceInfo->Longtitude,     Table.GetFieldValue("Longtitude"));
            pDeviceInfo->DeviceMode             = atoi(Table.GetFieldValue("DeviceMode"));
            pDeviceInfo->DirectionCode          = atoi(Table.GetFieldValue("DirectionCode"));
            pDeviceInfo->Lane                   = atoi(Table.GetFieldValue("Lane"));
            pDeviceInfo->SnapMode               = atoi(Table.GetFieldValue("SnapMode"));
            return SUCCESS;
        }
        else if (nRow == 0)
        {
            strcpy(pDeviceInfo->DeviceNumber,"LDR-8A-001");
            strcpy(pDeviceInfo->DeviceSite,"徽州大道");
            strcpy(pDeviceInfo->DirectionText,"由东向西");
            strcpy(pDeviceInfo->RoadCode,"001002003");
            strcpy(pDeviceInfo->DepartmentCode,"90001001");
            strcpy(pDeviceInfo->Latitude,"32.52N");
            strcpy(pDeviceInfo->Longtitude,"117.17E");
            pDeviceInfo->DeviceMode     = 5;
            pDeviceInfo->DirectionCode  = 2;
            pDeviceInfo->Lane           = 3;
            pDeviceInfo->SnapMode       = 2;

            setParaDeviceinfo(pDeviceInfo);

            return SUCCESS;
        }
        else
        {
            throw nRow;
        }
    }
    catch(SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
    catch(int nrow)
    {
        LOG("DeviceInfo Row is not valid:%d", nrow);
        return FAILED;
    }
}

int setParaDeviceinfo(DEVICE_INFO_T *pDeviceInfo)
{
    assert(pDeviceInfo != NULL);
    try
    {
        SQLite3Buffer Buff;

        Db.BeginTransaction();
        Db.ExecSQL("DELETE FROM DeviceInfo");

        Db.ExecSQL(Buff.format("INSERT INTO DeviceInfo VALUES( %Q,%Q,%Q,%Q,%Q,%Q,%Q,%d,%d,%d,%d);",
                    pDeviceInfo->DeviceNumber,
                    pDeviceInfo->DeviceSite,
                    pDeviceInfo->DirectionText,
                    pDeviceInfo->RoadCode,
                    pDeviceInfo->DepartmentCode,
                    pDeviceInfo->Latitude,
                    pDeviceInfo->Longtitude,
                    pDeviceInfo->DeviceMode,
                    pDeviceInfo->DirectionCode,
                    pDeviceInfo->Lane,
                    pDeviceInfo->SnapMode)
                );

        Db.CommitTransaction();

        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        if (Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int getParaRedlightzone(REDLIGHT_ZONE_T *pRedlightZone, int length)
{ 
    assert(pRedlightZone != NULL);
    try
    {
        SQLite3Table Table = Db.GetTable("SELECT * FROM RedlightZone");

        int nRow = Table.GetRowsNum();

        if (nRow >= length)
        {
            for (int i = 0; i < length; ++i)
            {
                Table.SetRow(i);
                (pRedlightZone+i)->Index    = atoi(Table.GetFieldValue("Idx"));
                (pRedlightZone+i)->Top      = atoi(Table.GetFieldValue("Top"));
                (pRedlightZone+i)->Left     = atoi(Table.GetFieldValue("Left"));
                (pRedlightZone+i)->Height   = atoi(Table.GetFieldValue("Height"));
                (pRedlightZone+i)->Width    = atoi(Table.GetFieldValue("Width"));
            }
            return SUCCESS;
        }
        else
        {
            for (int i = 0; i < length; ++i)
            {
                (pRedlightZone+i)->Index    = i;
                (pRedlightZone+i)->Top      = 400;
                (pRedlightZone+i)->Left     = 600;
                (pRedlightZone+i)->Height   = 200;
                (pRedlightZone+i)->Width    = 150;
            }

            return setParaRedlightzone(pRedlightZone, length);
        }
    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int setParaRedlightzone(REDLIGHT_ZONE_T *pRedlightZone, int length)
{
    assert(pRedlightZone != NULL);
    try
    {
        Db.BeginTransaction();
        Db.ExecSQL("DELETE FROM RedlightZone");

        SQLite3Statement Statement = Db.compileStatement("INSERT INTO RedlightZone VALUES(?,?,?,?,?)");

        for (int i = 0; i < length; ++i)
        {
            Statement.bind(1,(pRedlightZone+i)->Index);
            Statement.bind(2,(pRedlightZone+i)->Top);
            Statement.bind(3,(pRedlightZone+i)->Left);
            Statement.bind(4,(pRedlightZone+i)->Height);
            Statement.bind(5,(pRedlightZone+i)->Width);

            Statement.ExecSQL();
            Statement.reset();
        }

        Db.CommitTransaction();

        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        if (Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int setParaVideoinDefaultRow(int index)
{
    try
    {
        Db.BeginTransaction();


        SQLite3Buffer Buff;
        Db.ExecSQL(Buff.format("DELETE FROM VideoInBase WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM signalLostAlarmHandleType WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM motDetAlarmHandleType WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM maskAlarmHandleType WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM nameOsdUpLeft WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM logoUpLeft WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM hideArea WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM osdPara WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM compressionPara WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM netCompPara WHERE Idx=%d", index));
        Db.ExecSQL(Buff.format("DELETE FROM recordDay WHERE Idx=%d", index));

        Db.ExecSQL(Buff.format("INSERT INTO VideoInBase VALUES(%d, 'default',1,3,3,1,1,1,1,1,1,1,1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO signalLostAlarmHandleType VALUES(%d, 10,42)", index));
        Db.ExecSQL(Buff.format("INSERT INTO motDetAlarmHandleType VALUES(%d, 1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO maskAlarmHandleType VALUES(%d, 1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO nameOsdUpLeft VALUES(%d, 1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO logoUpLeft VALUES(%d, 1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO hideArea  VALUES(%d, 1,1,1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO osdPara VALUES(%d, 1,1,1,1,1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO compressionPara VALUES(%d, 1,1,1,1,1,1)", index));
        Db.ExecSQL(Buff.format("INSERT INTO netCompPara VALUES(%d, 1,1,1,1,1,1)", index));

        for (int j = 0; j < MAX_DAYS; ++j)
        {
            Db.ExecSQL(Buff.format("INSERT INTO recordDay VALUES(%d,%d,1,1)",index, j));
        }

        Db.CommitTransaction();

        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        if (Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}


int setParaVideoinDefault(int length)
{
    try
    {
        Db.BeginTransaction();

        Db.ExecSQL("DELETE FROM VideoInBase");
        Db.ExecSQL("DELETE FROM signalLostAlarmHandleType");
        Db.ExecSQL("DELETE FROM motDetAlarmHandleType");
        Db.ExecSQL("DELETE FROM maskAlarmHandleType");
        Db.ExecSQL("DELETE FROM nameOsdUpLeft");
        Db.ExecSQL("DELETE FROM logoUpLeft");
        Db.ExecSQL("DELETE FROM hideArea");
        Db.ExecSQL("DELETE FROM osdPara");
        Db.ExecSQL("DELETE FROM compressionPara");
        Db.ExecSQL("DELETE FROM netCompPara");
        Db.ExecSQL("DELETE FROM recordDay");

        SQLite3Statement Stmt_base      = Db.compileStatement("INSERT INTO VideoInBase VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");    
        SQLite3Statement Stmt_signalLost= Db.compileStatement("INSERT INTO signalLostAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_motDet    = Db.compileStatement("INSERT INTO motDetAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_maskAlarm = Db.compileStatement("INSERT INTO maskAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_nameOsd   = Db.compileStatement("INSERT INTO nameOsdUpLeft VALUES(?,?,?)");
        SQLite3Statement Stmt_logoUpLeft= Db.compileStatement("INSERT INTO logoUpLeft VALUES(?,?,?)");
        SQLite3Statement Stmt_hideArea  = Db.compileStatement("INSERT INTO hideArea VALUES(?,?,?,?,?)");
        SQLite3Statement Stmt_osdPara   = Db.compileStatement("INSERT INTO osdPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_compPara  = Db.compileStatement("INSERT INTO compressionPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_netComp   = Db.compileStatement("INSERT INTO netCompPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_recordDay = Db.compileStatement("INSERT INTO recordDay VALUES(?,?,?,?)");

        for (int i = 0; i < length; ++i)
        {
            Stmt_base.bind(1, i);
            Stmt_base.bind(2, "DefaultName");
            Stmt_base.bind(3, 1);
            Stmt_base.bind(4, 1);
            Stmt_base.bind(5, 1);
            Stmt_base.bind(6, 1);
            Stmt_base.bind(7, 1);
            Stmt_base.bind(8, 1);
            Stmt_base.bind(9, 1);
            Stmt_base.bind(10, 1);
            Stmt_base.bind(11, 1);
            Stmt_base.bind(12, 1);
            Stmt_base.bind(13, 1);
            Stmt_base.bind(14, 1);

            Stmt_signalLost.bind(1, i);
            Stmt_signalLost.bind(2, 0);
            Stmt_signalLost.bind(3, 1);

            Stmt_motDet.bind(1, i);
            Stmt_motDet.bind(2, 2);
            Stmt_motDet.bind(3, 0);

            Stmt_maskAlarm.bind(1, i);
            Stmt_maskAlarm.bind(2, 1);
            Stmt_maskAlarm.bind(3, 3);

            Stmt_nameOsd.bind(1, i);
            Stmt_nameOsd.bind(2, 20);
            Stmt_nameOsd.bind(3, 5);

            Stmt_logoUpLeft.bind(1, i);
            Stmt_logoUpLeft.bind(2, 900);
            Stmt_logoUpLeft.bind(3, 800);

            Stmt_hideArea.bind(1, i);
            Stmt_hideArea.bind(2, 400);
            Stmt_hideArea.bind(3, 400);
            Stmt_hideArea.bind(4, 20);
            Stmt_hideArea.bind(5, 25);

            Stmt_osdPara.bind(1, i);
            Stmt_osdPara.bind(2, 40);
            Stmt_osdPara.bind(3, 5);
            Stmt_osdPara.bind(4, 1);
            Stmt_osdPara.bind(5, 1);
            Stmt_osdPara.bind(6, 1);
            Stmt_osdPara.bind(7, 1);

            Stmt_compPara.bind(1, i);
            Stmt_compPara.bind(2, 1);
            Stmt_compPara.bind(3, 1);
            Stmt_compPara.bind(4, 100);
            Stmt_compPara.bind(5, 70);
            Stmt_compPara.bind(6, 10);
            Stmt_compPara.bind(7, 70);

            Stmt_netComp.bind(1, i);
            Stmt_netComp.bind(2, 1);
            Stmt_netComp.bind(3, 1);
            Stmt_netComp.bind(4, 90);
            Stmt_netComp.bind(5, 80);
            Stmt_netComp.bind(6, 10);
            Stmt_netComp.bind(7, 60);

            Stmt_base.ExecSQL();
            Stmt_signalLost.ExecSQL();
            Stmt_motDet.ExecSQL();    
            Stmt_maskAlarm.ExecSQL(); 
            Stmt_nameOsd.ExecSQL();   
            Stmt_logoUpLeft.ExecSQL();
            Stmt_hideArea.ExecSQL();  
            Stmt_osdPara.ExecSQL();   
            Stmt_compPara.ExecSQL();  
            Stmt_netComp.ExecSQL();   

            Stmt_base.reset();
            Stmt_signalLost.reset();
            Stmt_motDet.reset();    
            Stmt_maskAlarm.reset(); 
            Stmt_nameOsd.reset();
            Stmt_logoUpLeft.reset();
            Stmt_hideArea.reset();  
            Stmt_osdPara.reset();   
            Stmt_compPara.reset();  
            Stmt_netComp.reset();   

            for (int j = 0; j < MAX_DAYS; ++j)
            {
                Stmt_recordDay.bind(1, i);
                Stmt_recordDay.bind(2, j);
                Stmt_recordDay.bind(3, 1);
                Stmt_recordDay.bind(4, 5);

                Stmt_recordDay.ExecSQL();
                Stmt_recordDay.reset();
            }
        }

        Db.CommitTransaction();

        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        if (Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int getParaVideoinLBL(VIDEOIN *pVideoIn, int length)
{
    assert(pVideoIn != NULL);
    try
    {
        for (int i = 0; i < length; ++i)
        {
            SQLite3Buffer Buff;
            SQLite3Table T_base         = Db.GetTable(Buff.format("SELECT * FROM VideoInBase WHERE Idx=%d",i));
            SQLite3Table T_signalLost   = Db.GetTable(Buff.format("SELECT * FROM signalLostAlarmHandleType WHERE Idx=%d",i));
            SQLite3Table T_motDet       = Db.GetTable(Buff.format("SELECT * FROM motDetAlarmHandleType WHERE Idx=%d",i));
            SQLite3Table T_maskAlarm    = Db.GetTable(Buff.format("SELECT * FROM maskAlarmHandleType WHERE Idx=%d",i));
            SQLite3Table T_nameOsd      = Db.GetTable(Buff.format("SELECT * FROM nameOsdUpLeft WHERE Idx=%d",i));
            SQLite3Table T_logoUpLeft   = Db.GetTable(Buff.format("SELECT * FROM logoUpLeft WHERE Idx=%d",i));
            SQLite3Table T_hideArea     = Db.GetTable(Buff.format("SELECT * FROM hideArea WHERE Idx=%d",i));
            SQLite3Table T_osdPara      = Db.GetTable(Buff.format("SELECT * FROM osdPara WHERE Idx=%d",i));
            SQLite3Table T_compPara     = Db.GetTable(Buff.format("SELECT * FROM compressionPara WHERE Idx=%d",i));
            SQLite3Table T_netComp      = Db.GetTable(Buff.format("SELECT * FROM netCompPara WHERE Idx=%d",i));
            SQLite3Table T_recordDay    = Db.GetTable(Buff.format("SELECT * FROM recordDay WHERE Idx=%d",i));

            int baseRow = T_base.GetRowsNum();

            int recordDayCnt = Db.ExecScalar(Buff.format("SELECT COUNT(*) FROM recordDay WHERE Idx=%d and Idex=1",i));

            if (T_signalLost.GetRowsNum() != baseRow || T_motDet.GetRowsNum() != baseRow
                || T_maskAlarm.GetRowsNum() != baseRow || T_nameOsd.GetRowsNum() != baseRow
                || T_logoUpLeft.GetRowsNum() != baseRow || T_hideArea.GetRowsNum() != baseRow
                || T_osdPara.GetRowsNum() != baseRow || T_compPara.GetRowsNum() != baseRow
                || T_netComp.GetRowsNum() != baseRow || recordDayCnt != baseRow || 0 == baseRow)
            {
                memset((pVideoIn+i)->channelName, 0, NAME_LEN);
                memcpy((pVideoIn+i)->channelName, "default", strlen("default")+1 > NAME_LEN ? NAME_LEN : strlen("default")+1);
                (pVideoIn+i)->videoStandard     = 1; 
                (pVideoIn+i)->brightness        = 3;
                (pVideoIn+i)->contrast          = 3;
                (pVideoIn+i)->tonality          = 1;
                (pVideoIn+i)->saturation        = 1;
                (pVideoIn+i)->enableNameOsd     = 1;
                (pVideoIn+i)->logoAttrib        = 1;
                (pVideoIn+i)->enableHide        = 1;
                (pVideoIn+i)->enableSignalLostAlarm = 1;
                (pVideoIn+i)->enableMotDetAlarm = 1;
                (pVideoIn+i)->enableMaskAlarm   = 1;
                (pVideoIn+i)->enableRecord      = 1;

                std::cout << (pVideoIn+i)->channelName << ' ';
                std::cout << (int)(pVideoIn+i)->videoStandard << ' ';
                std::cout << (int)(pVideoIn+i)->brightness << ' ';       
                std::cout << (int)(pVideoIn+i)->contrast << ' ';         
                std::cout << (int)(pVideoIn+i)->tonality << ' ';         
                std::cout << (int)(pVideoIn+i)->saturation << ' ';       
                std::cout << (int)(pVideoIn+i)->enableNameOsd << ' ';
                std::cout << (int)(pVideoIn+i)->logoAttrib << ' ';       
                std::cout << (int)(pVideoIn+i)->enableHide << ' ';       
                std::cout << (int)(pVideoIn+i)->enableSignalLostAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMotDetAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMaskAlarm << ' ';  
                std::cout << (int)(pVideoIn+i)->enableRecord << std::endl;

                (pVideoIn+i)->signalLostAlarmHandleType.handleType          = 10;
                (pVideoIn+i)->signalLostAlarmHandleType.alarmOutTriggered   = 42;

                (pVideoIn+i)->motDetAlarmHandleType.handleType              = 1;
                (pVideoIn+i)->motDetAlarmHandleType.alarmOutTriggered       = 1;

                (pVideoIn+i)->maskAlarmHandleType.handleType                = 1;
                (pVideoIn+i)->maskAlarmHandleType.alarmOutTriggered         = 1;

                (pVideoIn+i)->nameOsdUpLeft.x       = 1;
                (pVideoIn+i)->nameOsdUpLeft.y       = 1;

                (pVideoIn+i)->logoUpLeft.x          = 1;
                (pVideoIn+i)->logoUpLeft.y          = 1;

                (pVideoIn+i)->hideArea.upLeft.x     = 1;
                (pVideoIn+i)->hideArea.upLeft.y     = 1;
                (pVideoIn+i)->hideArea.size.width   = 1;
                (pVideoIn+i)->hideArea.size.height  = 1; 

                (pVideoIn+i)->osdPara.upLeft.x      = 1; 
                (pVideoIn+i)->osdPara.upLeft.y      = 1; 
                (pVideoIn+i)->osdPara.osdType       = 1; 
                (pVideoIn+i)->osdPara.osdAttrib     = 1; 
                (pVideoIn+i)->osdPara.dispWeek      = 1; 
                (pVideoIn+i)->osdPara.enable_date   = 1; 

                (pVideoIn+i)->compressionPara.streamAttr        = 1; 
                (pVideoIn+i)->compressionPara.intervalFrameI    = 1; 
                (pVideoIn+i)->compressionPara.maxVideoBitrate   = 1; 
                (pVideoIn+i)->compressionPara.videoFrameRate    = 1; 
                (pVideoIn+i)->compressionPara.BFrameNum         = 1; 
                (pVideoIn+i)->compressionPara.quality           = 1; 

                (pVideoIn+i)->netCompPara.streamAttr            = 1; 
                (pVideoIn+i)->netCompPara.intervalFrameI        = 1; 
                (pVideoIn+i)->netCompPara.maxVideoBitrate       = 1; 
                (pVideoIn+i)->netCompPara.videoFrameRate        = 1; 
                (pVideoIn+i)->netCompPara.BFrameNum             = 1; 
                (pVideoIn+i)->netCompPara.quality               = 1; 

                for (int j = 0; j < MAX_DAYS; ++j)
                {
                    (pVideoIn+i)->recordDay[j].bAllDayRecord    = 1; 
                    (pVideoIn+i)->recordDay[j].recType          = 1;
                }

                setParaVideoinDefaultRow(i);
            }
            else
            {
                T_base.SetRow(0);
                T_signalLost.SetRow(0);
                T_motDet.SetRow(0);    
                T_maskAlarm.SetRow(0); 
                T_nameOsd.SetRow(0);   
                T_logoUpLeft.SetRow(0);
                T_hideArea.SetRow(0);  
                T_osdPara.SetRow(0);   
                T_compPara.SetRow(0);  
                T_netComp.SetRow(0);   

                const char *str = T_base.GetFieldValue("channelName");
                memset((pVideoIn+i)->channelName, 0, NAME_LEN);
                memcpy((pVideoIn+i)->channelName, str, strlen(str)+1 > NAME_LEN ? NAME_LEN : strlen(str)+1);
                (pVideoIn+i)->videoStandard     = UINT8(atoi(T_base.GetFieldValue("videoStandard")));
                (pVideoIn+i)->brightness        = UINT8(atoi(T_base.GetFieldValue("brightness")));
                (pVideoIn+i)->contrast          = UINT8(atoi(T_base.GetFieldValue("contrast")));
                (pVideoIn+i)->tonality          = UINT8(atoi(T_base.GetFieldValue("tonality")));
                (pVideoIn+i)->saturation        = UINT8(atoi(T_base.GetFieldValue("saturation")));
                (pVideoIn+i)->enableNameOsd     = UINT8(atoi(T_base.GetFieldValue("enableNameOsd")));
                (pVideoIn+i)->logoAttrib        = UINT8(atoi(T_base.GetFieldValue("logoAttrib")));
                (pVideoIn+i)->enableHide        = UINT8(atoi(T_base.GetFieldValue("enableHide")));
                (pVideoIn+i)->enableSignalLostAlarm = UINT8(atoi(T_base.GetFieldValue("enableSignalLostAlarm")));
                (pVideoIn+i)->enableMotDetAlarm = UINT8(atoi(T_base.GetFieldValue("enableMotDetAlarm")));
                (pVideoIn+i)->enableMaskAlarm   = UINT8(atoi(T_base.GetFieldValue("enableMaskAlarm")));
                (pVideoIn+i)->enableRecord      = UINT8(atoi(T_base.GetFieldValue("enableRecord")));

                std::cout << (pVideoIn+i)->channelName << ' ';
                std::cout << (int)(pVideoIn+i)->videoStandard << ' ';
                std::cout << (int)(pVideoIn+i)->brightness << ' ';       
                std::cout << (int)(pVideoIn+i)->contrast << ' ';         
                std::cout << (int)(pVideoIn+i)->tonality << ' ';         
                std::cout << (int)(pVideoIn+i)->saturation << ' ';       
                std::cout << (int)(pVideoIn+i)->enableNameOsd << ' ';
                std::cout << (int)(pVideoIn+i)->logoAttrib << ' ';       
                std::cout << (int)(pVideoIn+i)->enableHide << ' ';       
                std::cout << (int)(pVideoIn+i)->enableSignalLostAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMotDetAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMaskAlarm << ' ';  
                std::cout << (int)(pVideoIn+i)->enableRecord << std::endl;




                (pVideoIn+i)->signalLostAlarmHandleType.handleType          = atoi(T_signalLost.GetFieldValue("handleType"));
                (pVideoIn+i)->signalLostAlarmHandleType.alarmOutTriggered   = atoi(T_signalLost.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->motDetAlarmHandleType.handleType              = atoi(T_motDet.GetFieldValue("handleType"));
                (pVideoIn+i)->motDetAlarmHandleType.alarmOutTriggered       = atoi(T_signalLost.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->maskAlarmHandleType.handleType                = atoi(T_maskAlarm.GetFieldValue("handleType"));
                (pVideoIn+i)->maskAlarmHandleType.alarmOutTriggered         = atoi(T_maskAlarm.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->nameOsdUpLeft.x       = UINT16(atoi(T_nameOsd.GetFieldValue("x")));
                (pVideoIn+i)->nameOsdUpLeft.y       = UINT16(atoi(T_nameOsd.GetFieldValue("y")));

                (pVideoIn+i)->logoUpLeft.x          = UINT16(atoi(T_logoUpLeft.GetFieldValue("x")));
                (pVideoIn+i)->logoUpLeft.y          = UINT16(atoi(T_logoUpLeft.GetFieldValue("y")));

                (pVideoIn+i)->hideArea.upLeft.x     = UINT16(atoi(T_hideArea.GetFieldValue("x")));
                (pVideoIn+i)->hideArea.upLeft.y     = UINT16(atoi(T_hideArea.GetFieldValue("y")));
                (pVideoIn+i)->hideArea.size.width   = UINT16(atoi(T_hideArea.GetFieldValue("Width")));
                (pVideoIn+i)->hideArea.size.height  = UINT16(atoi(T_hideArea.GetFieldValue("Height")));

                (pVideoIn+i)->osdPara.upLeft.x      = UINT16(atoi(T_osdPara.GetFieldValue("x")));
                (pVideoIn+i)->osdPara.upLeft.y      = UINT16(atoi(T_osdPara.GetFieldValue("y")));
                (pVideoIn+i)->osdPara.osdType       = UINT8(atoi(T_osdPara.GetFieldValue("osdType")));
                (pVideoIn+i)->osdPara.osdAttrib     = UINT8(atoi(T_osdPara.GetFieldValue("osdAttrib")));
                (pVideoIn+i)->osdPara.dispWeek      = UINT8(atoi(T_osdPara.GetFieldValue("dispWeek")));
                (pVideoIn+i)->osdPara.enable_date   = UINT8(atoi(T_osdPara.GetFieldValue("enable_date")));

                (pVideoIn+i)->compressionPara.streamAttr        = UINT16(atoi(T_compPara.GetFieldValue("streamAttr")));
                (pVideoIn+i)->compressionPara.intervalFrameI    = UINT16(atoi(T_compPara.GetFieldValue("intervalFrameI")));
                (pVideoIn+i)->compressionPara.maxVideoBitrate   = UINT32(atoi(T_compPara.GetFieldValue("maxVideoBitrate")));
                (pVideoIn+i)->compressionPara.videoFrameRate    = UINT32(atoi(T_compPara.GetFieldValue("videoFrameRate")));
                (pVideoIn+i)->compressionPara.BFrameNum         = UINT8(atoi(T_compPara.GetFieldValue("BFrameNum")));
                (pVideoIn+i)->compressionPara.quality           = UINT16(atoi(T_compPara.GetFieldValue("quality")));

                (pVideoIn+i)->netCompPara.streamAttr            = UINT16(atoi(T_netComp.GetFieldValue("streamAttr")));
                (pVideoIn+i)->netCompPara.intervalFrameI        = UINT16(atoi(T_netComp.GetFieldValue("intervalFrameI")));
                (pVideoIn+i)->netCompPara.maxVideoBitrate       = UINT32(atoi(T_netComp.GetFieldValue("maxVideoBitrate")));
                (pVideoIn+i)->netCompPara.videoFrameRate        = UINT32(atoi(T_netComp.GetFieldValue("videoFrameRate")));
                (pVideoIn+i)->netCompPara.BFrameNum             = UINT8(atoi(T_netComp.GetFieldValue("BFrameNum")));
                (pVideoIn+i)->netCompPara.quality               = UINT16(atoi(T_netComp.GetFieldValue("quality")));


                for (int j = 0; j < MAX_DAYS; ++j)
                {
                    T_recordDay.SetRow(j);
                    (pVideoIn+i)->recordDay[j].bAllDayRecord    = UINT16(atoi(T_recordDay.GetFieldValue("bAllDayRecord")));
                    (pVideoIn+i)->recordDay[j].recType          = UINT8(atoi(T_recordDay.GetFieldValue("recType")));
                }
            }
        }
        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

int getParaVideoin(VIDEOIN *pVideoIn, int length)
{
    assert(pVideoIn != NULL);
    try
    {
        SQLite3Table T_base         = Db.GetTable("SELECT * FROM VideoInBase");
        SQLite3Table T_signalLost   = Db.GetTable("SELECT * FROM signalLostAlarmHandleType");
        SQLite3Table T_motDet       = Db.GetTable("SELECT * FROM motDetAlarmHandleType");
        SQLite3Table T_maskAlarm    = Db.GetTable("SELECT * FROM maskAlarmHandleType");
        SQLite3Table T_nameOsd      = Db.GetTable("SELECT * FROM nameOsdUpLeft");
        SQLite3Table T_logoUpLeft   = Db.GetTable("SELECT * FROM logoUpLeft");
        SQLite3Table T_hideArea     = Db.GetTable("SELECT * FROM hideArea");
        SQLite3Table T_osdPara      = Db.GetTable("SELECT * FROM osdPara");
        SQLite3Table T_compPara     = Db.GetTable("SELECT * FROM compressionPara");
        SQLite3Table T_netComp      = Db.GetTable("SELECT * FROM netCompPara");
        SQLite3Table T_recordDay    = Db.GetTable("SELECT * FROM recordDay");


        int baseRow = T_base.GetRowsNum();

        int recordDayCnt = Db.ExecScalar("SELECT COUNT(*) FROM recordDay WHERE Idex=1");

        if (T_signalLost.GetRowsNum() != baseRow || T_motDet.GetRowsNum() != baseRow
            || T_maskAlarm.GetRowsNum() != baseRow || T_nameOsd.GetRowsNum() != baseRow
            || T_logoUpLeft.GetRowsNum() != baseRow || T_hideArea.GetRowsNum() != baseRow
            || T_osdPara.GetRowsNum() != baseRow || T_compPara.GetRowsNum() != baseRow
            || T_netComp.GetRowsNum() != baseRow || recordDayCnt != baseRow)
        {
            return setParaVideoinDefault(length);
        }

        if (baseRow >= length)
        {
            for (int i = 0; i < length; ++i)
            {
                T_base.SetRow(i);
                T_signalLost.SetRow(i);
                T_motDet.SetRow(i);    
                T_maskAlarm.SetRow(i); 
                T_nameOsd.SetRow(i);   
                T_logoUpLeft.SetRow(i);
                T_hideArea.SetRow(i);  
                T_osdPara.SetRow(i);   
                T_compPara.SetRow(i);  
                T_netComp.SetRow(i);   

                const char *str = T_base.GetFieldValue("channelName");
                memset((pVideoIn+i)->channelName, 0, NAME_LEN);
                memcpy((pVideoIn+i)->channelName, str, strlen(str)+1 > NAME_LEN ? NAME_LEN : strlen(str)+1);
                (pVideoIn+i)->videoStandard     = UINT8(atoi(T_base.GetFieldValue("videoStandard")));
                (pVideoIn+i)->brightness        = UINT8(atoi(T_base.GetFieldValue("brightness")));
                (pVideoIn+i)->contrast          = UINT8(atoi(T_base.GetFieldValue("contrast")));
                (pVideoIn+i)->tonality          = UINT8(atoi(T_base.GetFieldValue("tonality")));
                (pVideoIn+i)->saturation        = UINT8(atoi(T_base.GetFieldValue("saturation")));
                (pVideoIn+i)->enableNameOsd     = UINT8(atoi(T_base.GetFieldValue("enableNameOsd")));
                (pVideoIn+i)->logoAttrib        = UINT8(atoi(T_base.GetFieldValue("logoAttrib")));
                (pVideoIn+i)->enableHide        = UINT8(atoi(T_base.GetFieldValue("enableHide")));
                (pVideoIn+i)->enableSignalLostAlarm = UINT8(atoi(T_base.GetFieldValue("enableSignalLostAlarm")));
                (pVideoIn+i)->enableMotDetAlarm = UINT8(atoi(T_base.GetFieldValue("enableMotDetAlarm")));
                (pVideoIn+i)->enableMaskAlarm   = UINT8(atoi(T_base.GetFieldValue("enableMaskAlarm")));
                (pVideoIn+i)->enableRecord      = UINT8(atoi(T_base.GetFieldValue("enableRecord")));

                std::cout << (pVideoIn+i)->channelName << ' ';
                std::cout << (int)(pVideoIn+i)->videoStandard << ' ';
                std::cout << (int)(pVideoIn+i)->brightness << ' ';       
                std::cout << (int)(pVideoIn+i)->contrast << ' ';         
                std::cout << (int)(pVideoIn+i)->tonality << ' ';         
                std::cout << (int)(pVideoIn+i)->saturation << ' ';       
                std::cout << (int)(pVideoIn+i)->enableNameOsd << ' ';
                std::cout << (int)(pVideoIn+i)->logoAttrib << ' ';       
                std::cout << (int)(pVideoIn+i)->enableHide << ' ';       
                std::cout << (int)(pVideoIn+i)->enableSignalLostAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMotDetAlarm << ' ';
                std::cout << (int)(pVideoIn+i)->enableMaskAlarm << ' ';  
                std::cout << (int)(pVideoIn+i)->enableRecord << std::endl;




                (pVideoIn+i)->signalLostAlarmHandleType.handleType          = atoi(T_signalLost.GetFieldValue("handleType"));
                (pVideoIn+i)->signalLostAlarmHandleType.alarmOutTriggered   = atoi(T_signalLost.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->motDetAlarmHandleType.handleType              = atoi(T_motDet.GetFieldValue("handleType"));
                (pVideoIn+i)->motDetAlarmHandleType.alarmOutTriggered       = atoi(T_signalLost.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->maskAlarmHandleType.handleType                = atoi(T_maskAlarm.GetFieldValue("handleType"));
                (pVideoIn+i)->maskAlarmHandleType.alarmOutTriggered         = atoi(T_maskAlarm.GetFieldValue("alarmOutTriggered"));

                (pVideoIn+i)->nameOsdUpLeft.x       = UINT16(atoi(T_nameOsd.GetFieldValue("x")));
                (pVideoIn+i)->nameOsdUpLeft.y       = UINT16(atoi(T_nameOsd.GetFieldValue("y")));

                (pVideoIn+i)->logoUpLeft.x          = UINT16(atoi(T_logoUpLeft.GetFieldValue("x")));
                (pVideoIn+i)->logoUpLeft.y          = UINT16(atoi(T_logoUpLeft.GetFieldValue("y")));

                (pVideoIn+i)->hideArea.upLeft.x     = UINT16(atoi(T_hideArea.GetFieldValue("x")));
                (pVideoIn+i)->hideArea.upLeft.y     = UINT16(atoi(T_hideArea.GetFieldValue("y")));
                (pVideoIn+i)->hideArea.size.width   = UINT16(atoi(T_hideArea.GetFieldValue("Width")));
                (pVideoIn+i)->hideArea.size.height  = UINT16(atoi(T_hideArea.GetFieldValue("Height")));

                (pVideoIn+i)->osdPara.upLeft.x      = UINT16(atoi(T_osdPara.GetFieldValue("x")));
                (pVideoIn+i)->osdPara.upLeft.y      = UINT16(atoi(T_osdPara.GetFieldValue("y")));
                (pVideoIn+i)->osdPara.osdType       = UINT8(atoi(T_osdPara.GetFieldValue("osdType")));
                (pVideoIn+i)->osdPara.osdAttrib     = UINT8(atoi(T_osdPara.GetFieldValue("osdAttrib")));
                (pVideoIn+i)->osdPara.dispWeek      = UINT8(atoi(T_osdPara.GetFieldValue("dispWeek")));
                (pVideoIn+i)->osdPara.enable_date   = UINT8(atoi(T_osdPara.GetFieldValue("enable_date")));

                (pVideoIn+i)->compressionPara.streamAttr        = UINT16(atoi(T_compPara.GetFieldValue("streamAttr")));
                (pVideoIn+i)->compressionPara.intervalFrameI    = UINT16(atoi(T_compPara.GetFieldValue("intervalFrameI")));
                (pVideoIn+i)->compressionPara.maxVideoBitrate   = UINT32(atoi(T_compPara.GetFieldValue("maxVideoBitrate")));
                (pVideoIn+i)->compressionPara.videoFrameRate    = UINT32(atoi(T_compPara.GetFieldValue("videoFrameRate")));
                (pVideoIn+i)->compressionPara.BFrameNum         = UINT8(atoi(T_compPara.GetFieldValue("BFrameNum")));
                (pVideoIn+i)->compressionPara.quality           = UINT16(atoi(T_compPara.GetFieldValue("quality")));

                (pVideoIn+i)->netCompPara.streamAttr            = UINT16(atoi(T_netComp.GetFieldValue("streamAttr")));
                (pVideoIn+i)->netCompPara.intervalFrameI        = UINT16(atoi(T_netComp.GetFieldValue("intervalFrameI")));
                (pVideoIn+i)->netCompPara.maxVideoBitrate       = UINT32(atoi(T_netComp.GetFieldValue("maxVideoBitrate")));
                (pVideoIn+i)->netCompPara.videoFrameRate        = UINT32(atoi(T_netComp.GetFieldValue("videoFrameRate")));
                (pVideoIn+i)->netCompPara.BFrameNum             = UINT8(atoi(T_netComp.GetFieldValue("BFrameNum")));
                (pVideoIn+i)->netCompPara.quality               = UINT16(atoi(T_netComp.GetFieldValue("quality")));


                for (int j = 0; j < MAX_DAYS; ++j)
                {
                    T_recordDay.SetRow(i*MAX_DAYS+j);
                    (pVideoIn+i)->recordDay[j].bAllDayRecord    = UINT16(atoi(T_recordDay.GetFieldValue("bAllDayRecord")));
                    (pVideoIn+i)->recordDay[j].recType          = UINT8(atoi(T_recordDay.GetFieldValue("recType")));
                }
            }

            return SUCCESS;
        }
        else 
        {
            return setParaVideoinDefault(length);
        }

    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}


int setParaVideoin(VIDEOIN *pVideoIn, int length)
{
    assert(pVideoIn != NULL);
    try
    {
        Db.BeginTransaction();

        Db.ExecSQL("DELETE FROM VideoInBase");
        Db.ExecSQL("DELETE FROM signalLostAlarmHandleType");
        Db.ExecSQL("DELETE FROM motDetAlarmHandleType");
        Db.ExecSQL("DELETE FROM maskAlarmHandleType");
        Db.ExecSQL("DELETE FROM nameOsdUpLeft");
        Db.ExecSQL("DELETE FROM logoUpLeft");
        Db.ExecSQL("DELETE FROM hideArea");
        Db.ExecSQL("DELETE FROM osdPara");
        Db.ExecSQL("DELETE FROM compressionPara");
        Db.ExecSQL("DELETE FROM netCompPara");
        Db.ExecSQL("DELETE FROM recordDay");

        SQLite3Statement Stmt_base      = Db.compileStatement("INSERT INTO VideoInBase VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");    
        SQLite3Statement Stmt_signalLost= Db.compileStatement("INSERT INTO signalLostAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_motDet    = Db.compileStatement("INSERT INTO motDetAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_maskAlarm = Db.compileStatement("INSERT INTO maskAlarmHandleType VALUES(?,?,?)");
        SQLite3Statement Stmt_nameOsd   = Db.compileStatement("INSERT INTO nameOsdUpLeft VALUES(?,?,?)");
        SQLite3Statement Stmt_logoUpLeft= Db.compileStatement("INSERT INTO logoUpLeft VALUES(?,?,?)");
        SQLite3Statement Stmt_hideArea  = Db.compileStatement("INSERT INTO hideArea VALUES(?,?,?,?,?)");
        SQLite3Statement Stmt_osdPara   = Db.compileStatement("INSERT INTO osdPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_compPara  = Db.compileStatement("INSERT INTO compressionPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_netComp   = Db.compileStatement("INSERT INTO netCompPara VALUES(?,?,?,?,?,?,?)");
        SQLite3Statement Stmt_recordDay = Db.compileStatement("INSERT INTO recordDay VALUES(?,?,?,?)");

        for (int i = 0; i < length; ++i)
        {
            Stmt_base.bind(1,i);
            Stmt_base.bind(2, (char *)(pVideoIn+i)->channelName);
            Stmt_base.bind(3, (pVideoIn+i)->videoStandard);
            Stmt_base.bind(4, (pVideoIn+i)->brightness);
            Stmt_base.bind(5, (pVideoIn+i)->contrast);
            Stmt_base.bind(6, (pVideoIn+i)->tonality);
            Stmt_base.bind(7, (pVideoIn+i)->saturation);
            Stmt_base.bind(8, (pVideoIn+i)->enableNameOsd);
            Stmt_base.bind(9, (pVideoIn+i)->logoAttrib);
            Stmt_base.bind(10, (pVideoIn+i)->enableHide);
            Stmt_base.bind(11, (pVideoIn+i)->enableSignalLostAlarm);
            Stmt_base.bind(12, (pVideoIn+i)->enableMotDetAlarm);
            Stmt_base.bind(13, (pVideoIn+i)->enableMaskAlarm);
            Stmt_base.bind(14, (pVideoIn+i)->enableRecord);

            Stmt_signalLost.bind(1, i);
            Stmt_signalLost.bind(2, (int)(pVideoIn+i)->signalLostAlarmHandleType.handleType);
            Stmt_signalLost.bind(3, (int)(pVideoIn+i)->signalLostAlarmHandleType.alarmOutTriggered);

            Stmt_motDet.bind(1, i);
            Stmt_motDet.bind(2, (int)(pVideoIn+i)->motDetAlarmHandleType.handleType);
            Stmt_motDet.bind(3, (int)(pVideoIn+i)->motDetAlarmHandleType.alarmOutTriggered);

            Stmt_maskAlarm.bind(1, i);
            Stmt_maskAlarm.bind(2, (int)(pVideoIn+i)->maskAlarmHandleType.handleType);
            Stmt_maskAlarm.bind(3, (int)(pVideoIn+i)->maskAlarmHandleType.alarmOutTriggered);

            Stmt_nameOsd.bind(1, i);
            Stmt_nameOsd.bind(2, (int)(pVideoIn+i)->nameOsdUpLeft.x);
            Stmt_nameOsd.bind(3, (pVideoIn+i)->nameOsdUpLeft.y);

            Stmt_logoUpLeft.bind(1, i);
            Stmt_logoUpLeft.bind(2, (pVideoIn+i)->logoUpLeft.x);
            Stmt_logoUpLeft.bind(3, (pVideoIn+i)->logoUpLeft.y);

            Stmt_hideArea.bind(1, i);
            Stmt_hideArea.bind(2, (pVideoIn+i)->hideArea.upLeft.x);
            Stmt_hideArea.bind(3, (pVideoIn+i)->hideArea.upLeft.y);
            Stmt_hideArea.bind(4, (pVideoIn+i)->hideArea.size.width);
            Stmt_hideArea.bind(5, (pVideoIn+i)->hideArea.size.height);

            Stmt_osdPara.bind(1, i);
            Stmt_osdPara.bind(2, (pVideoIn+i)->osdPara.upLeft.x);   
            Stmt_osdPara.bind(3, (pVideoIn+i)->osdPara.upLeft.y);   
            Stmt_osdPara.bind(4, (pVideoIn+i)->osdPara.osdType);    
            Stmt_osdPara.bind(5, (pVideoIn+i)->osdPara.osdAttrib);  
            Stmt_osdPara.bind(6, (pVideoIn+i)->osdPara.dispWeek);   
            Stmt_osdPara.bind(7, (pVideoIn+i)->osdPara.enable_date);

            Stmt_compPara.bind(1, i);
            Stmt_compPara.bind(2, (int)(pVideoIn+i)->compressionPara.streamAttr);
            Stmt_compPara.bind(3, (int)(pVideoIn+i)->compressionPara.intervalFrameI);
            Stmt_compPara.bind(4, (int)(pVideoIn+i)->compressionPara.maxVideoBitrate);
            Stmt_compPara.bind(5, (int)(pVideoIn+i)->compressionPara.videoFrameRate);
            Stmt_compPara.bind(6, (int)(pVideoIn+i)->compressionPara.BFrameNum);
            Stmt_compPara.bind(7, (int)(pVideoIn+i)->compressionPara.quality);

            Stmt_netComp.bind(1, i);
            Stmt_netComp.bind(2, (int)(pVideoIn+i)->netCompPara.streamAttr);
            Stmt_netComp.bind(3, (int)(pVideoIn+i)->netCompPara.intervalFrameI);
            Stmt_netComp.bind(4, (int)(pVideoIn+i)->netCompPara.maxVideoBitrate);
            Stmt_netComp.bind(5, (int)(pVideoIn+i)->netCompPara.videoFrameRate);
            Stmt_netComp.bind(6, (int)(pVideoIn+i)->netCompPara.BFrameNum);
            Stmt_netComp.bind(7, (int)(pVideoIn+i)->netCompPara.quality);

            Stmt_base.ExecSQL();
            Stmt_signalLost.ExecSQL();
            Stmt_motDet.ExecSQL();    
            Stmt_maskAlarm.ExecSQL(); 
            Stmt_nameOsd.ExecSQL();   
            Stmt_logoUpLeft.ExecSQL();
            Stmt_hideArea.ExecSQL();  
            Stmt_osdPara.ExecSQL();   
            Stmt_compPara.ExecSQL();  
            Stmt_netComp.ExecSQL();   

            Stmt_base.reset();
            Stmt_signalLost.reset();
            Stmt_motDet.reset();    
            Stmt_maskAlarm.reset(); 
            Stmt_nameOsd.reset();
            Stmt_logoUpLeft.reset();
            Stmt_hideArea.reset();  
            Stmt_osdPara.reset();   
            Stmt_compPara.reset();  
            Stmt_netComp.reset();   

            for (int j = 0; j < MAX_DAYS; ++j)
            {
                Stmt_recordDay.bind(1, i);
                Stmt_recordDay.bind(2, j);
                Stmt_recordDay.bind(3, (pVideoIn+i)->recordDay[j].bAllDayRecord);
                Stmt_recordDay.bind(4, (pVideoIn+i)->recordDay[j].recType);

                Stmt_recordDay.ExecSQL();
                Stmt_recordDay.reset();
            }
        }

        Db.CommitTransaction();

        return SUCCESS;
    }
    catch (SQLite3Exception &e)
    {
        if (Db.IsDoingTransaction())
        {
            Db.RollbackTransaction();
        }
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}

#if 0
int getParaVideoin(VIDEOIN *pVideoIn, int length)
{
    try
    {
        SQLite3Statement Stmt_base      = Db.compileStatement("SELECT * FROM VideoInBase WHERE Idx=?");
        SQLite3Statement Stmt_signalLost= Db.compileStatement("SELECT * FROM signalLostAlarmHandleType WHERE Idx=?");
        SQLite3Statement Stmt_motDet    = Db.compileStatement("SELECT * FROM motDetAlarmHandleType WHERE Idx=?");
        SQLite3Statement Stmt_maskAlarm = Db.compileStatement("SELECT * FROM maskAlarmHandleType WHERE Idx=?");
        SQLite3Statement Stmt_nameOsd   = Db.compileStatement("SELECT * FROM nameOsdUpLeft WHERE Idx=?");
        SQLite3Statement Stmt_logoUpLeft= Db.compileStatement("SELECT * FROM logoUpLeft WHERE Idx=?");
        SQLite3Statement Stmt_hideArea  = Db.compileStatement("SELECT * FROM hideArea WHERE Idx=?");
        SQLite3Statement Stmt_osdPara   = Db.compileStatement("SELECT * FROM osdPara WHERE Idx=?");
        SQLite3Statement Stmt_compPara  = Db.compileStatement("SELECT * FROM compressionPara WHERE Idx=?");
        SQLite3Statement Stmt_netComp   = Db.compileStatement("SELECT * FROM netCompPara WHERE Idx=?");
        SQLite3Statement Stmt_recordDay = Db.compileStatement("SELECT * FROM recordDay WHERE Idx=?");

        for (int i = 0; i < length; ++i)
        {
            Stmt_base.bind(1, i);      
            Stmt_signalLost.bind(1,i);
            Stmt_motDet.bind(1,i);    
            Stmt_maskAlarm.bind(1,i); 
            Stmt_nameOsd.bind(1,i);   
            Stmt_logoUpLeft.bind(1,i);
            Stmt_hideArea.bind(1,i);  
            Stmt_osdPara.bind(1,i);   
            Stmt_compPara.bind(1,i);  
            Stmt_netComp.bind(1,i);   
            Stmt_recordDay.bind(1,i); 

            SQLite3Query Q_base         = Stmt_base.execQuery();
            SQLite3Query Q_signalLost   = Stmt_signalLost.execQuery();
            SQLite3Query Q_motDet       = Stmt_motDet.execQuery();
            SQLite3Query Q_maskAlarm    = Stmt_maskAlarm.execQuery();
            SQLite3Query Q_nameOsd      = Stmt_nameOsd.execQuery();
            SQLite3Query Q_logoUpLeft   = Stmt_logoUpLeft.execQuery();
            SQLite3Query Q_hideArea     = Stmt_hideArea.execQuery();
            SQLite3Query Q_osdPara      = Stmt_osdPara.execQuery();
            SQLite3Query Q_compPara     = Stmt_compPara.execQuery();
            SQLite3Query Q_netComp      = Stmt_netComp.execQuery();

            const char *str = Q_base.GetFieldValue("channelName");
            printf("str is %s\n", str);
            memcpy((pVideoIn+i)->channelName, str, strlen(str)+1 > NAME_LEN ? NAME_LEN : strlen(str+1));
            (pVideoIn+i)->videoStandard     = UINT8(atoi(Q_base.GetFieldValue("videoStandard")));
            (pVideoIn+i)->brightness        = UINT8(atoi(Q_base.GetFieldValue("brightness")));
            (pVideoIn+i)->contrast          = UINT8(atoi(Q_base.GetFieldValue("contrast")));
            (pVideoIn+i)->tonality          = UINT8(atoi(Q_base.GetFieldValue("tonality")));
            (pVideoIn+i)->saturation        = UINT8(atoi(Q_base.GetFieldValue("saturation")));
            (pVideoIn+i)->enableNameOsd     = UINT8(atoi(Q_base.GetFieldValue("enableNameOsd")));
            (pVideoIn+i)->logoAttrib        = UINT8(atoi(Q_base.GetFieldValue("logoAttrib")));
            (pVideoIn+i)->enableHide        = UINT8(atoi(Q_base.GetFieldValue("enableHide")));
            (pVideoIn+i)->enableSignalLostAlarm = UINT8(atoi(Q_base.GetFieldValue("enableSignalLostAlarm")));
            (pVideoIn+i)->enableMotDetAlarm = UINT8(atoi(Q_base.GetFieldValue("enableMotDetAlarm")));
            (pVideoIn+i)->enableMaskAlarm   = UINT8(atoi(Q_base.GetFieldValue("enableMaskAlarm")));
            (pVideoIn+i)->enableRecord      = UINT8(atoi(Q_base.GetFieldValue("enableRecord")));

            (pVideoIn+i)->signalLostAlarmHandleType.handleType          = atoi(Q_signalLost.GetFieldValue("handleType"));
            (pVideoIn+i)->signalLostAlarmHandleType.alarmOutTriggered   = atoi(Q_signalLost.GetFieldValue("alarmOutTriggered"));

            (pVideoIn+i)->motDetAlarmHandleType.handleType              = atoi(Q_motDet.GetFieldValue("handleType"));
            (pVideoIn+i)->motDetAlarmHandleType.alarmOutTriggered       = atoi(Q_signalLost.GetFieldValue("alarmOutTriggered"));

            (pVideoIn+i)->maskAlarmHandleType.handleType                = atoi(Q_maskAlarm.GetFieldValue("handleType"));
            (pVideoIn+i)->maskAlarmHandleType.alarmOutTriggered         = atoi(Q_maskAlarm.GetFieldValue("alarmOutTriggered"));

            (pVideoIn+i)->nameOsdUpLeft.x       = UINT16(atoi(Q_nameOsd.GetFieldValue("x")));
            (pVideoIn+i)->nameOsdUpLeft.y       = UINT16(atoi(Q_nameOsd.GetFieldValue("y")));

            (pVideoIn+i)->logoUpLeft.x          = UINT16(atoi(Q_logoUpLeft.GetFieldValue("x")));
            (pVideoIn+i)->logoUpLeft.y          = UINT16(atoi(Q_logoUpLeft.GetFieldValue("y")));

            (pVideoIn+i)->hideArea.upLeft.x     = UINT16(atoi(Q_hideArea.GetFieldValue("x")));
            (pVideoIn+i)->hideArea.upLeft.y     = UINT16(atoi(Q_hideArea.GetFieldValue("y")));
            (pVideoIn+i)->hideArea.size.width   = UINT16(atoi(Q_hideArea.GetFieldValue("Width")));
            (pVideoIn+i)->hideArea.size.height  = UINT16(atoi(Q_hideArea.GetFieldValue("Height")));

            (pVideoIn+i)->osdPara.upLeft.x      = UINT16(atoi(Q_osdPara.GetFieldValue("x")));
            (pVideoIn+i)->osdPara.upLeft.y      = UINT16(atoi(Q_osdPara.GetFieldValue("y")));
            (pVideoIn+i)->osdPara.osdType       = UINT8(atoi(Q_osdPara.GetFieldValue("osdType")));
            (pVideoIn+i)->osdPara.osdAttrib     = UINT8(atoi(Q_osdPara.GetFieldValue("osdAttrib")));
            (pVideoIn+i)->osdPara.dispWeek      = UINT8(atoi(Q_osdPara.GetFieldValue("dispWeek")));
            (pVideoIn+i)->osdPara.enable_date   = UINT8(atoi(Q_osdPara.GetFieldValue("enable_date")));

            (pVideoIn+i)->compressionPara.streamAttr        = UINT16(atoi(Q_compPara.GetFieldValue("streamAttr")));
            (pVideoIn+i)->compressionPara.intervalFrameI    = UINT16(atoi(Q_compPara.GetFieldValue("intervalFrameI")));
            (pVideoIn+i)->compressionPara.maxVideoBitrate   = UINT32(atoi(Q_compPara.GetFieldValue("maxVideoBitrate")));
            (pVideoIn+i)->compressionPara.videoFrameRate    = UINT32(atoi(Q_compPara.GetFieldValue("videoFrameRate")));
            (pVideoIn+i)->compressionPara.BFrameNum         = UINT8(atoi(Q_compPara.GetFieldValue("BFrameNum")));
            (pVideoIn+i)->compressionPara.quality           = UINT16(atoi(Q_compPara.GetFieldValue("quality")));

            (pVideoIn+i)->netCompPara.streamAttr            = UINT16(atoi(Q_netComp.GetFieldValue("streamAttr")));
            (pVideoIn+i)->netCompPara.intervalFrameI        = UINT16(atoi(Q_netComp.GetFieldValue("intervalFrameI")));
            (pVideoIn+i)->netCompPara.maxVideoBitrate       = UINT32(atoi(Q_netComp.GetFieldValue("maxVideoBitrate")));
            (pVideoIn+i)->netCompPara.videoFrameRate        = UINT32(atoi(Q_netComp.GetFieldValue("videoFrameRate")));
            (pVideoIn+i)->netCompPara.BFrameNum             = UINT8(atoi(Q_netComp.GetFieldValue("BFrameNum")));
            (pVideoIn+i)->netCompPara.quality               = UINT16(atoi(Q_netComp.GetFieldValue("quality")));

            SQLite3Query Q_recordDay    = Stmt_recordDay.execQuery();
            for (int j = 0; j < MAX_DAYS; ++j)
            {
                (pVideoIn+i)->recordDay[j].bAllDayRecord = UINT16(atoi(Q_recordDay.GetFieldValue("bAllDayRecord")));
                (pVideoIn+i)->recordDay[j].recType = UINT8(atoi(Q_recordDay.GetFieldValue("recType")));
                if (Q_recordDay.Eof())
                {
                    break;
                }
                Q_recordDay.NextRow();
            }

            Stmt_base.reset();      
            Stmt_signalLost.reset();
            Stmt_motDet.reset();    
            Stmt_maskAlarm.reset(); 
            Stmt_nameOsd.reset();   
            Stmt_logoUpLeft.reset();
            Stmt_hideArea.reset();  
            Stmt_osdPara.reset();   
            Stmt_compPara.reset();  
            Stmt_netComp.reset();   
            Stmt_recordDay.reset(); 
        }
        return SUCCESS;

    }
    catch (SQLite3Exception &e)
    {
        LOG("%s:%s", e.GetErrorCodeAsString(e.GetErrorCode()), e.GetErrorDesc());
        return FAILED;
    }
}
#endif
