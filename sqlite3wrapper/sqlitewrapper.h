////////////////////////////////////////////////////////////////////////////////
// CppSQLite3 - A C++ wrapper around the SQLite3 embedded database library.
//////////////////////////////////////////////////////////////////////////////

#ifndef _SQLite3WARPPER_H_
#define _SQLite3WARPPER_H_

#include "sqlite3.h"
#include <cstdio>
#include <cstring>
#ifndef WIN32
#include <pthread.h>
#endif


#define  SQLITE_WRAPPER_ERROR    1000
#define	 SQLITE_MEM_OFFSET_SIZE		256

class SQLite3Exception
{
public:
	
    SQLite3Exception(const int nErrCode, const char* szErrMess, bool bDeleteMsg=true);
    SQLite3Exception(const SQLite3Exception&  e);	
    virtual ~SQLite3Exception();

    int GetErrorCode() const { return m_dwErrCode; }	
    const char* GetErrorDesc() const { return m_pszErrDesc; }	
    static const char* GetErrorCodeAsString(int dwErrCode);
	
private:
    int    m_dwErrCode;
    char*  m_pszErrDesc;
};


class SQLite3Buffer
{
public:
	
    SQLite3Buffer();
    ~SQLite3Buffer();
	
    const char* format(const char* szFormat, ...);
    operator const char*() { return m_pBuffer; }
    void clear();
	
private:
	
    char* m_pBuffer;
};

class SQLite3Mutex
{
public:
    SQLite3Mutex()
    {
        m_mutex = sqlite3_mutex_alloc(0);
        
        if (NULL == m_mutex)
        {
            throw  SQLite3Exception(0, "Alloc Mutex Error", true);
        }
    }
    ~SQLite3Mutex()
    {
        sqlite3_mutex_free(m_mutex);
        m_mutex = NULL;
    }

    void Lock(void)
    {
        sqlite3_mutex_enter(m_mutex);
    }

    void Unlock(void)
    {
        sqlite3_mutex_leave(m_mutex);
    }

    void Trylock(void)
    {
        sqlite3_mutex_try(m_mutex);
    }

private:
    sqlite3_mutex *m_mutex;
};

class SQLite3Query
{
public:
	
    SQLite3Query();
    SQLite3Query(const SQLite3Query& rQuery);
    SQLite3Query(sqlite3* pDB, sqlite3_stmt* pVM, bool bEof, bool bOwnVM=true);
    SQLite3Query& operator=(const SQLite3Query& rQuery);
    virtual ~SQLite3Query();
	
    int GetFieldsNum();
    int GetFieldIndex(const char* szField);
    const char* GetFieldName(int nCol);
    const char* GetFieldDeclType(int nCol);
    int GetFieldDataType(int nCol);
    const char* GetFieldValue(int nField);
    const char* GetFieldValue(const char* szField);
    int GetIntField(int nField, int nNullValue=0);
    int GetIntField(const char* szField, int nNullValue=0);
    double GetFloatField(int nField, double fNullValue=0.0);
    double GetFloatField(const char* szField, double fNullValue=0.0);
    const char* GetStringField(int nField, const char* szNullValue="");
    const char* GetStringField(const char* szField, const char* szNullValue="");
    const unsigned char* GetBlobField(int nField, int& nLen);
    const unsigned char* GetBlobField(const char* szField, int& nLen);
    bool GetFieldIsNull(int nField);
    bool GetFieldIsNull(const char* szField);
    bool Eof();
    void NextRow();
    void Finalize();
	
private:
	
    void CheckVM();
	
	sqlite3*      m_pDB;
    sqlite3_stmt* m_pVM;
    bool          m_bEof;
    int           m_dwCols;
    bool          m_bOwnVM;
};

class SQLite3Table
{
public:
	
    SQLite3Table();
    SQLite3Table(const SQLite3Table& rTable);
    SQLite3Table(char** paszResults, int nRows, int nCols);
    virtual ~SQLite3Table();
	
    SQLite3Table& operator=(const SQLite3Table& rTable);
    int GetFieldsNum();
    int  GetRowsNum();
    const char* GetFieldName(int nCol);
    const char* GetFieldValue(int nField);
    const char* GetFieldValue(const char* szField);
    int GetIntField(int nField, int nNullValue=0);
    int GetIntField(const char* szField, int nNullValue=0);
    double GetFloatField(int nField, double fNullValue=0.0);
    double GetFloatField(const char* szField, double fNullValue=0.0);
    const char* GetStringField(int nField, const char* szNullValue="");
    const char* GetStringField(const char* szField, const char* szNullValue="");
    bool GetFieldIsNull(int nField);
    bool GetFieldIsNull(const char* szField);
    void SetRow(int nRow);
    void Finalize();
	
private:
    void CheckResults();
    int     m_dwCols;
    int     m_dwRows;
    int     m_dwCurrentRow;
    char**  m_paszResults;
};

class SQLite3Statement
{
public:

    SQLite3Statement();
    SQLite3Statement(const SQLite3Statement& rStatement);
    SQLite3Statement(sqlite3* pDB, sqlite3_stmt* pVM);
    virtual ~SQLite3Statement();

    SQLite3Statement& operator=(const SQLite3Statement& rStatement);
    int ExecSQL();
    SQLite3Query execQuery();
    void bind(int nParam, const char* szValue);
    void bind(int nParam, const int nValue);
    void bind(int nParam, const double dwValue);
    void bind(int nParam, const unsigned char* blobValue, int nLen);
    void bindNull(int nParam);
    void reset();
    void finalize();

private:

    void CheckDB();
    void CheckVM();

    sqlite3* m_pDB;
    sqlite3_stmt* m_pVM;
};


class SQLite3DB
{
public:

    SQLite3DB();
    virtual ~SQLite3DB();

    void OpenDb(const char* szFile);
    void CloseDb();
	bool TableExists(const char* szTable);
    int          BeginTransaction(void);
    int          CommitTransaction(void);	
    int          RollbackTransaction(void);	
	bool         IsDoingTransaction();
	int          ExecSQL(const char* szSQL);
	SQLite3Query ExecQuery(const char* szSQL);
    int          ExecScalar(const char* szSQL);
    SQLite3Table GetTable(const char* szSQL);
    SQLite3Statement compileStatement(const char* szSQL);
    sqlite_int64 LastRowId();
    void Interrupt() { sqlite3_interrupt(m_pDB); }
    void SetBusyTimeout(int nMillisecs);
    static const char* SQLiteVersion() { return SQLITE_VERSION; }

    void Backup(const char *BackupFile);

#ifdef _SQLMEMCHK_
	bool CheckOffsetMemZero();
#endif

private:

    SQLite3DB(const SQLite3DB& db);
    SQLite3DB& operator=(const SQLite3DB& db);
    sqlite3_stmt* compile(const char* szSQL);

    void CheckDB();
	void AddLockToDb();
	void ReleaseLockFromDb();

#ifdef _SQLMEMCHK_
	char  achMemF[SQLITE_MEM_OFFSET_SIZE];
#endif
    sqlite3* m_pDB;
	sqlite3* m_pBakDB;		//用于原套接字被异常关闭时
    int      m_dwBusyTimeoutMs;
	bool     m_bDoingTransaction;
#ifndef WIN32
	bool	 m_bDbLocked;
	pthread_mutex_t mutex;
#endif
#ifdef _SQLMEMCHK_
	char  achMemB[SQLITE_MEM_OFFSET_SIZE];
#endif
};

#endif
