#include "sqlitewrapper.h"
#include <cstdlib>
#ifndef WIN32
#include <unistd.h>
#include <errno.h>
#endif

#define BACKUP_PAGECOUNT 10

static const bool DONT_DELETE_MSG=false;

SQLite3Exception::SQLite3Exception(const int nErrCode,
									const char* szErrMess,
									bool bDeleteMsg/*=true*/) 
{
	m_dwErrCode = nErrCode;
	m_pszErrDesc = sqlite3_mprintf("%s[%d]: %s",
						GetErrorCodeAsString(nErrCode),
						nErrCode,
						szErrMess ? szErrMess : "");

	if (bDeleteMsg && szErrMess)
	{
		sqlite3_free((void *)szErrMess);
	}
}

									
SQLite3Exception::SQLite3Exception(const SQLite3Exception&  e) 
{
	m_dwErrCode = e.m_dwErrCode;
	m_pszErrDesc = 0;
	if (e.m_pszErrDesc)
	{
		m_pszErrDesc = sqlite3_mprintf("%s", e.m_pszErrDesc);
	}
}


const char* SQLite3Exception::GetErrorCodeAsString(int nErrCode)
{
	switch (nErrCode)
	{
		case SQLITE_OK          : return "SQLITE_OK";
		case SQLITE_ERROR       : return "SQLITE_ERROR";
		case SQLITE_INTERNAL    : return "SQLITE_INTERNAL";
		case SQLITE_PERM        : return "SQLITE_PERM";
		case SQLITE_ABORT       : return "SQLITE_ABORT";
		case SQLITE_BUSY        : return "SQLITE_BUSY";
		case SQLITE_LOCKED      : return "SQLITE_LOCKED";
		case SQLITE_NOMEM       : return "SQLITE_NOMEM";
		case SQLITE_READONLY    : return "SQLITE_READONLY";
		case SQLITE_INTERRUPT   : return "SQLITE_INTERRUPT";
		case SQLITE_IOERR       : return "SQLITE_IOERR";
		case SQLITE_CORRUPT     : return "SQLITE_CORRUPT";
		case SQLITE_NOTFOUND    : return "SQLITE_NOTFOUND";
		case SQLITE_FULL        : return "SQLITE_FULL";
		case SQLITE_CANTOPEN    : return "SQLITE_CANTOPEN";
		case SQLITE_PROTOCOL    : return "SQLITE_PROTOCOL";
		case SQLITE_EMPTY       : return "SQLITE_EMPTY";
		case SQLITE_SCHEMA      : return "SQLITE_SCHEMA";
		case SQLITE_TOOBIG      : return "SQLITE_TOOBIG";
		case SQLITE_CONSTRAINT  : return "SQLITE_CONSTRAINT";
		case SQLITE_MISMATCH    : return "SQLITE_MISMATCH";
		case SQLITE_MISUSE      : return "SQLITE_MISUSE";
		case SQLITE_NOLFS       : return "SQLITE_NOLFS";
		case SQLITE_AUTH        : return "SQLITE_AUTH";
		case SQLITE_FORMAT      : return "SQLITE_FORMAT";
		case SQLITE_RANGE       : return "SQLITE_RANGE";
		case SQLITE_ROW         : return "SQLITE_ROW";
		case SQLITE_DONE        : return "SQLITE_DONE";	
		case SQLITE_WRAPPER_ERROR: return "SQLITE_WRAPPER_ERROR";	
		default: return "UNKNOWN_ERROR";
	}
}


SQLite3Exception::~SQLite3Exception()
{
	if (m_pszErrDesc)
	{
		sqlite3_free(m_pszErrDesc);
		m_pszErrDesc = 0;
	}
}


SQLite3Buffer::SQLite3Buffer()
{
	m_pBuffer = 0;
}


SQLite3Buffer::~SQLite3Buffer()
{
	clear();
}


void SQLite3Buffer::clear()
{
	if (m_pBuffer)
	{
		sqlite3_free(m_pBuffer);
		m_pBuffer = 0;
	}

}


const char* SQLite3Buffer::format(const char* szFormat, ...)
{
	clear();
	va_list va;
	va_start(va, szFormat);
	m_pBuffer = sqlite3_vmprintf(szFormat, va);
	va_end(va);
	return m_pBuffer;
}

////////////////////////////////////////////////////////////////////////////////


SQLite3Query::SQLite3Query()
{
	m_pDB = 0;
	m_pVM = 0;
	m_bEof = true;
	m_dwCols = 0;
	m_bOwnVM = false;
}


SQLite3Query::SQLite3Query(const SQLite3Query& rQuery)
{
    m_pDB = rQuery.m_pDB;
	m_pVM = rQuery.m_pVM;
	// Only one object can own the VM
	const_cast<SQLite3Query&>(rQuery).m_pVM = 0;
	m_bEof = rQuery.m_bEof;
	m_dwCols = rQuery.m_dwCols;
	m_bOwnVM = rQuery.m_bOwnVM;
}


SQLite3Query::SQLite3Query(sqlite3* pDB,
							sqlite3_stmt* pVM,
							bool bEof,
							bool bOwnVM/*=true*/)
{
	m_pDB = pDB;
	m_pVM = pVM;
	m_bEof = bEof;
	m_dwCols = sqlite3_column_count(m_pVM); //函数返回结果集中包含的列数
	//sqlite3_column_count() 可以在执行了 sqlite3_prepare()之后的任何时刻调用
	m_bOwnVM = bOwnVM;
}


SQLite3Query::~SQLite3Query()
{
	try
	{
		Finalize();
	}
	catch (...)
	{
	}
}


SQLite3Query& SQLite3Query::operator=(const SQLite3Query& rQuery)
{
	try
	{
		Finalize();
	}
	catch (...)
	{
	}
	m_pVM = rQuery.m_pVM;
	// Only one object can own the VM
	const_cast<SQLite3Query&>(rQuery).m_pVM = 0;
	m_bEof = rQuery.m_bEof;
	m_dwCols = rQuery.m_dwCols;
	m_bOwnVM = rQuery.m_bOwnVM;
	return *this;
}


int SQLite3Query::GetFieldsNum()
{
	CheckVM();
	return m_dwCols;
}
const char* SQLite3Query::GetFieldValue(int nField)
{
	CheckVM();

	if (nField < 0 || nField > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}

	return (const char*)sqlite3_column_text(m_pVM, nField); //返回字段的值
}


const char* SQLite3Query::GetFieldValue(const char* szField)
{
	int nField = GetFieldIndex(szField);
	return (const char*)sqlite3_column_text(m_pVM, nField);//返回所在字段的 UTF-8 编码的 TEXT 数据
}

/*
** The next group of routines returns information about the information
** in a single column of the current result row of a query.  In every
** case the first parameter is a pointer to the SQL statement that is being
** executed (the sqlite_stmt* that was returned from sqlite3_prepare()) and
** the second argument is the index of the column for which information 
** should be returned.  iCol is zero-indexed.  The left-most column as an
** index of 0.
**
** If the SQL statement is not currently point to a valid row, or if the
** the colulmn index is out of range, the result is undefined.
**
** These routines attempt to convert the value where appropriate.  For
** example, if the internal representation is FLOAT and a text result
** is requested, sprintf() is used internally to do the conversion
** automatically.  The following table details the conversions that
** are applied:
**
**    Internal Type    Requested Type     Conversion
**    -------------    --------------    --------------------------
**       NULL             INTEGER         Result is 0
**       NULL             FLOAT           Result is 0.0
**       NULL             TEXT            Result is an empty string
**       NULL             BLOB            Result is a zero-length BLOB
**       INTEGER          FLOAT           Convert from integer to float
**       INTEGER          TEXT            ASCII rendering of the integer
**       INTEGER          BLOB            Same as for INTEGER->TEXT
**       FLOAT            INTEGER         Convert from float to integer
**       FLOAT            TEXT            ASCII rendering of the float
**       FLOAT            BLOB            Same as FLOAT->TEXT
**       TEXT             INTEGER         Use atoi()
**       TEXT             FLOAT           Use atof()
**       TEXT             BLOB            No change
**       BLOB             INTEGER         Convert to TEXT then use atoi()
**       BLOB             FLOAT           Convert to TEXT then use atof()
**       BLOB             TEXT            Add a \000 terminator if needed
**
** The following access routines are provided:
**
** _type()     Return the datatype of the result.  This is one of
**             SQLITE_INTEGER, SQLITE_FLOAT, SQLITE_TEXT, SQLITE_BLOB,
**             or SQLITE_NULL.
** _blob()     Return the value of a BLOB.
** _bytes()    Return the number of bytes in a BLOB value or the number
**             of bytes in a TEXT value represented as UTF-8.  The \000
**             terminator is included in the byte count for TEXT values.
** _bytes16()  Return the number of bytes in a BLOB value or the number
**             of bytes in a TEXT value represented as UTF-16.  The \u0000
**             terminator is included in the byte count for TEXT values.
** _double()   Return a FLOAT value.
** _int()      Return an INTEGER value in the host computer's native
**             integer representation.  This might be either a 32- or 64-bit
**             integer depending on the host.
** _int64()    Return an INTEGER value as a 64-bit signed integer.
** _text()     Return the value as UTF-8 text.
** _text16()   Return the value as UTF-16 text.
*/
int SQLite3Query::GetIntField(int nField, int nNullValue/*=0*/)
{
	if (GetFieldDataType(nField) == SQLITE_NULL)
	{
		return nNullValue;
	}
	else
	{
		return sqlite3_column_int(m_pVM, nField);
		//以本地主机的整数格式返回一个整数值
	}
}


int SQLite3Query::GetIntField(const char* szField, int nNullValue/*=0*/)
{
	int nField = GetFieldIndex(szField);
	return GetIntField(nField, nNullValue);
}


double SQLite3Query::GetFloatField(int nField, double fNullValue/*=0.0*/)
{
	if (GetFieldDataType(nField) == SQLITE_NULL)
	{
		return fNullValue;
	}
	else
	{
		return sqlite3_column_double(m_pVM, nField);
		//sqlite3_column_double() 返回浮点数
	}
}


double SQLite3Query::GetFloatField(const char* szField, double fNullValue/*=0.0*/)
{
	int nField = GetFieldIndex(szField);
	return GetFloatField(nField, fNullValue);
}


const char* SQLite3Query::GetStringField(int nField, const char* szNullValue/*=""*/)
{
	if (GetFieldDataType(nField) == SQLITE_NULL)
	{
		return szNullValue;
	}
	else
	{
		return (const char*)sqlite3_column_text(m_pVM, nField);
	}
}

const char* SQLite3Query::GetStringField(const char* szField, const char* szNullValue/*=""*/)
{
	int nField = GetFieldIndex(szField);
	return GetStringField(nField, szNullValue);
}


const unsigned char* SQLite3Query::GetBlobField(int nField, int& nLen)
{
	CheckVM();

	if (nField < 0 || nField > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}

	nLen = sqlite3_column_bytes(m_pVM, nField);
	return (const unsigned char*)sqlite3_column_blob(m_pVM, nField);
	//返回 BLOB 数据
}


const unsigned char* SQLite3Query::GetBlobField(const char* szField, int& nLen)
{
	int nField = GetFieldIndex(szField);
	return GetBlobField(nField, nLen);
}


bool SQLite3Query::GetFieldIsNull(int nField)
{    //判断字段是否为空
	return (GetFieldDataType(nField) == SQLITE_NULL);
}


bool SQLite3Query::GetFieldIsNull(const char* szField)
{
	int nField = GetFieldIndex(szField);
	return (GetFieldDataType(nField) == SQLITE_NULL);
}


int SQLite3Query::GetFieldIndex(const char* szField)
{
	CheckVM();

	if (szField)
	{
		for (int nField = 0; nField < m_dwCols; nField++)
		{
			const char* szTemp = sqlite3_column_name(m_pVM, nField);
			//返回第N列的字段名

			if (strcmp(szField, szTemp) == 0)
			{
				return nField;
			}
		}
	}

	throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
							"Invalid field name requested",
							DONT_DELETE_MSG);
}


const char* SQLite3Query::GetFieldName(int nCol)
{
	CheckVM();

	if (nCol < 0 || nCol > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}
	//返回第N列的字段名
	return sqlite3_column_name(m_pVM, nCol);
}


const char* SQLite3Query::GetFieldDeclType(int nCol)
{
	CheckVM();

	if (nCol < 0 || nCol > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}
//则用来返回该列在 CREATE TABLE 语句中声明的类型. 它可以用在当返回类型是空字符串的时
	return sqlite3_column_decltype(m_pVM, nCol);

	//sqlite3_column_bytes用来返回 UTF-8 编码的BLOBs列的字节数或者TEXT字符串的字节数
}


int SQLite3Query::GetFieldDataType(int nCol)
{
	CheckVM();

	if (nCol < 0 || nCol > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}
    //sqlite3_column_type()函数返回第N列的值的数据类型
	/*#define SQLITE_INTEGER  1
       #define SQLITE_FLOAT    2
       #define SQLITE_TEXT     3
       #define SQLITE_BLOB     4
       #define SQLITE_NULL     5
	   */

	return sqlite3_column_type(m_pVM, nCol);
}


bool SQLite3Query::Eof()
{
	CheckVM();
	return m_bEof;
}


void SQLite3Query::NextRow()
{
	CheckVM();

	int nRet = sqlite3_step(m_pVM);

	if (nRet == SQLITE_DONE)
	{
		// no rows
		m_bEof = true;
	}
	else if (nRet == SQLITE_ROW)
	{
		// more rows, nothing to do
	}
	else
	{
		const char* szError = sqlite3_errmsg(m_pDB);

		sqlite3_finalize(m_pVM);
		
		m_pVM = 0;
		
		throw SQLite3Exception(nRet,
								szError,
								DONT_DELETE_MSG);
	}
}


void SQLite3Query::Finalize()
{
	if (m_pVM && m_bOwnVM)
	{
		int nRet = sqlite3_finalize(m_pVM);
		m_pVM = 0;
		if (nRet != SQLITE_OK)
		{
			const char* szError = sqlite3_errmsg(m_pDB);
			throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
		}
	}
}


void SQLite3Query::CheckVM()
{
	if (m_pVM == 0)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Null Virtual Machine pointer",
								DONT_DELETE_MSG);
	}
}
//////////////////////////////////////////////

SQLite3Table::SQLite3Table()
{

	m_paszResults = 0;
	m_dwRows = 0;
	m_dwCols = 0;
	m_dwCurrentRow = 0;
}


SQLite3Table::SQLite3Table(const SQLite3Table& rTable)
{
	m_paszResults = rTable.m_paszResults;
	// Only one object can own the results
	const_cast<SQLite3Table&>(rTable).m_paszResults = 0;
	m_dwRows = rTable.m_dwRows;
	m_dwCols = rTable.m_dwCols;
	m_dwCurrentRow = rTable.m_dwCurrentRow;
}


SQLite3Table::SQLite3Table(char** paszResults, int nRows, int nCols)
{
	m_paszResults = paszResults;
	m_dwRows = nRows;
	m_dwCols = nCols;
	m_dwCurrentRow = 0;
}


SQLite3Table::~SQLite3Table()
{
	try
	{
		Finalize();
	}
	catch (...)
	{
	}
}


SQLite3Table& SQLite3Table::operator=(const SQLite3Table& rTable)
{
	try
	{
		Finalize();
	}
	catch (...)
	{
	}
	m_paszResults = rTable.m_paszResults;
	// Only one object can own the results
	const_cast<SQLite3Table&>(rTable).m_paszResults = 0;
	m_dwRows = rTable.m_dwRows;
	m_dwCols = rTable.m_dwCols;
	m_dwCurrentRow = rTable.m_dwCurrentRow;
	return *this;
}


void SQLite3Table::Finalize()
{
	if (m_paszResults)
	{
		sqlite3_free_table(m_paszResults);
		m_paszResults = 0;
	}
}


int SQLite3Table::GetFieldsNum()
{
	CheckResults();
	return m_dwCols;
}


int SQLite3Table::GetRowsNum()
{
	CheckResults();
	return m_dwRows;
}


const char* SQLite3Table::GetFieldValue(int nField)
{
	CheckResults();

	if (nField < 0 || nField > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}

	int nIndex = (m_dwCurrentRow*m_dwCols) + m_dwCols + nField;
	return m_paszResults[nIndex];
}


const char* SQLite3Table::GetFieldValue(const char* szField)
{
	CheckResults();

	if (szField)
	{
		for (int nField = 0; nField < m_dwCols; nField++)
		{
			if (strcmp(szField, m_paszResults[nField]) == 0)
			{
				int nIndex = (m_dwCurrentRow*m_dwCols) + m_dwCols + nField;
				return m_paszResults[nIndex];
			}
		}
	}

	throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
							"Invalid field name requested",
							DONT_DELETE_MSG);
}


int SQLite3Table::GetIntField(int nField, int nNullValue/*=0*/)
{
	if (GetFieldIsNull(nField))
	{
		return nNullValue;
	}
	else
	{
		return atoi(GetFieldValue(nField));
	}
}


int SQLite3Table::GetIntField(const char* szField, int nNullValue/*=0*/)
{
	if (GetFieldIsNull(szField))
	{
		return nNullValue;
	}
	else
	{
		return atoi(GetFieldValue(szField));
	}
}


double SQLite3Table::GetFloatField(int nField, double fNullValue/*=0.0*/)
{
	if (GetFieldIsNull(nField))
	{
		return fNullValue;
	}
	else
	{
		return atof(GetFieldValue(nField));
	}
}


double SQLite3Table::GetFloatField(const char* szField, double fNullValue/*=0.0*/)
{
	if (GetFieldIsNull(szField))
	{
		return fNullValue;
	}
	else
	{
		return atof(GetFieldValue(szField));
	}
}


const char* SQLite3Table::GetStringField(int nField, const char* szNullValue/*=""*/)
{
	if (GetFieldIsNull(nField))
	{
		return szNullValue;
	}
	else
	{
		return GetFieldValue(nField);
	}
}


const char* SQLite3Table::GetStringField(const char* szField, const char* szNullValue/*=""*/)
{
	if (GetFieldIsNull(szField))
	{
		return szNullValue;
	}
	else
	{
		return GetFieldValue(szField);
	}
}


bool SQLite3Table::GetFieldIsNull(int nField)
{
	CheckResults();
	return (GetFieldValue(nField) == 0);
}


bool SQLite3Table::GetFieldIsNull(const char* szField)
{
	CheckResults();
	return (GetFieldValue(szField) == 0);
}


const char* SQLite3Table::GetFieldName(int nCol)
{
	CheckResults();

	if (nCol < 0 || nCol > m_dwCols-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid field index requested",
								DONT_DELETE_MSG);
	}

	return m_paszResults[nCol];
}


void SQLite3Table::SetRow(int nRow)
{
	CheckResults();

	if (nRow < 0 || nRow > m_dwRows-1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid row index requested",
								DONT_DELETE_MSG);
	}

	m_dwCurrentRow = nRow;
}


void SQLite3Table::CheckResults()
{
	if (m_paszResults == 0)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Null Results pointer",
								DONT_DELETE_MSG);
	}
}


///////////////////////////////////////////////////


SQLite3Statement::SQLite3Statement()
{
	m_pDB = 0;
	m_pVM = 0;
}


SQLite3Statement::SQLite3Statement(const SQLite3Statement& rStatement)
{
	m_pDB = rStatement.m_pDB;
	m_pVM = rStatement.m_pVM;
	// Only one object can own VM
	const_cast<SQLite3Statement&>(rStatement).m_pVM = 0;
}


SQLite3Statement::SQLite3Statement(sqlite3* pDB, sqlite3_stmt* pVM)
{
	m_pDB = pDB;
	m_pVM = pVM;
}


SQLite3Statement::~SQLite3Statement()
{
	try
	{
		finalize();
	}
	catch (...)
	{
	}
}


SQLite3Statement& SQLite3Statement::operator=(const SQLite3Statement& rStatement)
{
	m_pDB = rStatement.m_pDB;
	m_pVM = rStatement.m_pVM;
	// Only one object can own VM
	const_cast<SQLite3Statement&>(rStatement).m_pVM = 0;
	return *this;
}


int SQLite3Statement::ExecSQL()
{
	CheckDB();
	CheckVM();

	const char* szError=0;

	int nRet = sqlite3_step(m_pVM);

	if (nRet == SQLITE_DONE)
	{
		int nRowsChanged = sqlite3_changes(m_pDB);

		nRet = sqlite3_reset(m_pVM);

		if (nRet != SQLITE_OK)
		{
			szError = sqlite3_errmsg(m_pDB);
			throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
		}

		return nRowsChanged;
	}
	else
	{
		szError = sqlite3_errmsg(m_pDB);
		sqlite3_reset(m_pVM);
		throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
	}
}


SQLite3Query SQLite3Statement::execQuery()
{
	CheckDB();
	CheckVM();

	int nRet = sqlite3_step(m_pVM);

	if (nRet == SQLITE_DONE)
	{
		// no rows
		return SQLite3Query(m_pDB, m_pVM, true/*eof*/, false);
	}
	else if (nRet == SQLITE_ROW)
	{
		// at least 1 row
		return SQLite3Query(m_pDB, m_pVM, false/*eof*/, false);
	}
	else
	{
		const char* szError = sqlite3_errmsg(m_pDB);
		sqlite3_reset(m_pVM);
		throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
	}
}


void SQLite3Statement::bind(int nParam, const char* szValue)
{
	CheckVM();
	int nRes = sqlite3_bind_text(m_pVM, nParam, szValue, -1, SQLITE_TRANSIENT);

	if (nRes != SQLITE_OK)
	{
		throw SQLite3Exception(nRes,
								"Error binding string param",
								DONT_DELETE_MSG);
	}
}


void SQLite3Statement::bind(int nParam, const int nValue)
{
	CheckVM();
	int nRes = sqlite3_bind_int(m_pVM, nParam, nValue);

	if (nRes != SQLITE_OK)
	{
		throw SQLite3Exception(nRes,
								"Error binding int param",
								DONT_DELETE_MSG);
	}
}


void SQLite3Statement::bind(int nParam, const double dValue)
{
	CheckVM();
	int nRes = sqlite3_bind_double(m_pVM, nParam, dValue);

	if (nRes != SQLITE_OK)
	{
		throw SQLite3Exception(nRes,
								"Error binding double param",
								DONT_DELETE_MSG);
	}
}


void SQLite3Statement::bind(int nParam, const unsigned char* blobValue, int nLen)
{
	CheckVM();
	int nRes = sqlite3_bind_blob(m_pVM, nParam,
								(const void*)blobValue, nLen, SQLITE_TRANSIENT);

	if (nRes != SQLITE_OK)
	{
		throw SQLite3Exception(nRes,
								"Error binding blob param",
								DONT_DELETE_MSG);
	}
}

	
void SQLite3Statement::bindNull(int nParam)
{
	CheckVM();
	int nRes = sqlite3_bind_null(m_pVM, nParam);

	if (nRes != SQLITE_OK)
	{
		throw SQLite3Exception(nRes,
								"Error binding NULL param",
								DONT_DELETE_MSG);
	}
}


void SQLite3Statement::reset()
{
	if (m_pVM)
	{
		int nRet = sqlite3_reset(m_pVM);

		if (nRet != SQLITE_OK)
		{
			const char* szError = sqlite3_errmsg(m_pDB);
			throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
		}
	}
}


void SQLite3Statement::finalize()
{
	if (m_pVM)
	{
		int nRet = sqlite3_finalize(m_pVM);
		m_pVM = 0;

		if (nRet != SQLITE_OK)
		{
			const char* szError = sqlite3_errmsg(m_pDB);
			throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
		}
	}
}


void SQLite3Statement::CheckDB()
{
	if (m_pDB == 0)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Database not open",
								DONT_DELETE_MSG);
	}
}


void SQLite3Statement::CheckVM()
{
	if (m_pVM == 0)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Null Virtual Machine pointer",
								DONT_DELETE_MSG);
	}
}


//////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

SQLite3DB::SQLite3DB()
{
	m_pDB = 0;
	m_dwBusyTimeoutMs = 60000; // 60 seconds
	m_bDoingTransaction = false;
	m_pBakDB = 0;
#ifndef WIN32
#ifndef _NVR2820_
	m_bDbLocked = false;
// 	pthread_mutexattr_t mattr;
// 	pthread_mutexattr_setprotocol(&mattr, PTHREAD_PRIO_INHERIT);
// 	pthread_mutexattr_setrobust_np(&mattr, PTHREAD_MUTEX_ROBUST_NP);
	pthread_mutex_init(&mutex, NULL);
#endif
#endif
#ifdef _SQLMEMCHK_
	memset(achMemF, 0, SQLITE_MEM_OFFSET_SIZE);
	memset(achMemB, 0, SQLITE_MEM_OFFSET_SIZE);
#endif
}


SQLite3DB::SQLite3DB(const SQLite3DB& db)
{
	m_pDB = db.m_pDB;
	m_dwBusyTimeoutMs = 60000; // 60 seconds
	m_bDoingTransaction = db.m_bDoingTransaction;
	m_pBakDB = db.m_pBakDB;
#ifndef WIN32
#ifndef _NVR2820_
	m_bDbLocked = false;
// 	pthread_mutexattr_t mattr;
// 	pthread_mutexattr_setprotocol(&mattr, PTHREAD_PRIO_INHERIT);
// 	pthread_mutexattr_setrobust_np(&mattr, PTHREAD_MUTEX_ROBUST_NP);
	pthread_mutex_init(&mutex, NULL);
	printf("init2 mutex:%p\n", &mutex);
#endif
#endif
#ifdef _SQLMEMCHK_
	memset(achMemF, 0, SQLITE_MEM_OFFSET_SIZE);
	memset(achMemB, 0, SQLITE_MEM_OFFSET_SIZE);
#endif
}


SQLite3DB::~SQLite3DB()
{
	CloseDb();
#ifndef WIN32
#ifndef _NVR2820_
 	pthread_mutex_destroy(&mutex);
#endif
#endif
}


SQLite3DB& SQLite3DB::operator=(const SQLite3DB& db)
{
	m_pDB = db.m_pDB;
	m_dwBusyTimeoutMs = 60000; // 60 seconds
	m_bDoingTransaction = db.m_bDoingTransaction;
	m_pBakDB = db.m_pBakDB;
#ifndef WIN32
#ifndef _NVR2820_
	m_bDbLocked = false;
// 	pthread_mutexattr_t mattr;
// 	pthread_mutexattr_setprotocol(&mattr, PTHREAD_PRIO_INHERIT);
// 	pthread_mutexattr_setrobust_np(&mattr, PTHREAD_MUTEX_ROBUST_NP);
	pthread_mutex_init(&mutex, NULL);
#endif
#endif
	return *this;
}

#ifdef _SQLMEMCHK_
bool SQLite3DB::CheckOffsetMemZero()
{
	char achZeroMem[SQLITE_MEM_OFFSET_SIZE] = {0};

	memset(achZeroMem, 0, SQLITE_MEM_OFFSET_SIZE);
	if ((memcmp(achZeroMem, achMemF, SQLITE_MEM_OFFSET_SIZE) != 0) ||
		(memcmp(achZeroMem, achMemB, SQLITE_MEM_OFFSET_SIZE) != 0))
	{
		return false;
	}
	return true;
}
#endif

void SQLite3DB::OpenDb(const char* szFile)
{
	AddLockToDb();
	int nRet = sqlite3_open(szFile, &m_pDB);
	ReleaseLockFromDb();

	if (nRet != SQLITE_OK)
	{
		AddLockToDb();
		const char* szError = sqlite3_errmsg(m_pDB);
		ReleaseLockFromDb();
		throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
	}
	if (m_pDB != m_pBakDB)
	{
		m_pBakDB = m_pDB;
	}

	SetBusyTimeout(m_dwBusyTimeoutMs);
}


void SQLite3DB::CloseDb()
{
	if (m_pDB)
	{
		AddLockToDb();
		int nRet = sqlite3_close(m_pDB);
		ReleaseLockFromDb();
		if (nRet != SQLITE_OK)
		{
			AddLockToDb();
			const char *szError = sqlite3_errmsg(m_pDB);
			ReleaseLockFromDb();
			throw SQLite3Exception(nRet, szError,DONT_DELETE_MSG);
		}
		m_pDB = 0;
		m_pBakDB = 0;
	}
}


SQLite3Statement SQLite3DB::compileStatement(const char* szSQL)
{
	CheckDB();

	sqlite3_stmt* pVM = compile(szSQL);
	return SQLite3Statement(m_pDB, pVM);
}


bool SQLite3DB::TableExists(const char* szTable)
{
	char szSQL[128];
	sprintf(szSQL,
			"select count(*) from sqlite_master where type='table' and name='%s'",
			szTable);

	int nRet = ExecScalar(szSQL);
	return (nRet > 0);
}

int  SQLite3DB::BeginTransaction(void)
{  
	int nRet = ExecSQL("BEGIN IMMEDIATE TRANSACTION;");
//	if(SQLITE_OK == nRet)
//	{
		m_bDoingTransaction = true;
// 	}
	return nRet;
	
}
int  SQLite3DB::CommitTransaction(void)
{
	int nRet =	ExecSQL("commit transaction;");
//	if(SQLITE_OK == nRet)
//	{
		m_bDoingTransaction = false;
// 	}
	return nRet;
}
int  SQLite3DB::RollbackTransaction(void)
{
	int nRet = ExecSQL("rollback transaction;");
//	if(SQLITE_OK == nRet)
//	{
		m_bDoingTransaction = false;
// 	}
	return nRet;
}
bool  SQLite3DB::IsDoingTransaction(void)
{
	return	m_bDoingTransaction;

}
int SQLite3DB::ExecSQL(const char* szSQL)
{
	CheckDB();

	const char* szError=0;
	AddLockToDb();
	int nRet = sqlite3_exec(m_pDB, szSQL, 0, 0, 0);
	ReleaseLockFromDb();

	if (nRet == SQLITE_OK)
	{
		AddLockToDb();
		int ret = sqlite3_changes(m_pDB);
		ReleaseLockFromDb();
		return ret;
	}
	else
	{
		AddLockToDb();
		szError = sqlite3_errmsg(m_pDB);
		ReleaseLockFromDb();
		throw SQLite3Exception(nRet, szError,DONT_DELETE_MSG);
	}
}


SQLite3Query SQLite3DB::ExecQuery(const char* szSQL)
{
	CheckDB();

	sqlite3_stmt* pVM = compile(szSQL);

	AddLockToDb();
	int nRet = sqlite3_step(pVM);
	ReleaseLockFromDb();

	if (nRet == SQLITE_DONE)
	{// 已成功完成。返回成功
		// no rows
		return SQLite3Query(m_pDB, pVM, true/*eof*/);
	}
	else if (nRet == SQLITE_ROW)
	{
		// at least 1 row
		//至少还有一行数据
		return SQLite3Query(m_pDB, pVM, false/*eof*/);
	}
	else
	{
		AddLockToDb();
		const char* szError= sqlite3_errmsg(m_pDB);
		ReleaseLockFromDb();
		AddLockToDb();
		sqlite3_finalize(pVM);
		ReleaseLockFromDb();
		throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
	}
}

int SQLite3DB::ExecScalar(const char* szSQL)
{
	SQLite3Query q = ExecQuery(szSQL);

	if (q.Eof() || q.GetFieldsNum() < 1)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Invalid scalar query",
								DONT_DELETE_MSG);
	}

	return atoi(q.GetFieldValue(0));
}

SQLite3Table SQLite3DB::GetTable(const char* szSQL)
{
	CheckDB();

	const char* szError=0;
	char** paszResults=0;
	int nRet;
	int nRows(0);
	int nCols(0);

	AddLockToDb();
	nRet = sqlite3_get_table(m_pDB, szSQL, &paszResults, &nRows, &nCols, 0);
	ReleaseLockFromDb();

	if (nRet == SQLITE_OK)
	{
		return SQLite3Table(paszResults, nRows, nCols);
	}
	else
	{
		AddLockToDb();
		szError = sqlite3_errmsg(m_pDB);
		ReleaseLockFromDb();
		throw SQLite3Exception(nRet, szError, DONT_DELETE_MSG);
	}
}


sqlite_int64 SQLite3DB::LastRowId()
{
	CheckDB();

	AddLockToDb();
	sqlite_int64 ret = sqlite3_last_insert_rowid(m_pDB);
	ReleaseLockFromDb();
	return ret;
}


void SQLite3DB::SetBusyTimeout(int nMillisecs)
{
	m_dwBusyTimeoutMs = nMillisecs;
	AddLockToDb();
	sqlite3_busy_timeout(m_pDB, m_dwBusyTimeoutMs);
	ReleaseLockFromDb();
}


void SQLite3DB::CheckDB()
{
	if (!m_pDB)
	{
		throw SQLite3Exception(SQLITE_WRAPPER_ERROR,
								"Database not open",
								DONT_DELETE_MSG);
	}
	if (m_pDB != m_pBakDB)
	{		
		m_pDB = m_pBakDB;
	}
}


sqlite3_stmt* SQLite3DB::compile(const char* szSQL)
{
	CheckDB();

	const char* szError=0;
	const char* szTail=0;
	sqlite3_stmt* pVM;

	AddLockToDb();
	int nRet = sqlite3_prepare(m_pDB, szSQL, -1, &pVM, &szTail);
	ReleaseLockFromDb();

	if (nRet != SQLITE_OK)
	{
		AddLockToDb();
		szError = sqlite3_errmsg(m_pDB);
		ReleaseLockFromDb();
		throw SQLite3Exception(nRet, szError,DONT_DELETE_MSG);
	}

	return pVM;
}

void SQLite3DB::AddLockToDb()
{
#ifndef WIN32
#ifndef _NVR2820_
	int ret = pthread_mutex_lock(&mutex);
	if (ret == EOWNERDEAD)
	{
		printf("pthread_mutex_lock failed1 %d\n", ret);
		//pthread_mutex_consistent_np(&mutex);
		pthread_mutex_unlock(&mutex);
		return;
	}
	else if (ret == ENOTRECOVERABLE)
	{
		printf("pthread_mutex_lock failed2 %d\n", ret);
		pthread_mutex_destroy(&mutex);
		return;
	}
	else
	{
		//printf("pthread_mutex_lock %d %p\n", ret, &mutex);
	}
	while (m_bDbLocked)
	{
		sleep(1);
	}
	m_bDbLocked = true;
	if (!ret)
	{
		ret = pthread_mutex_unlock(&mutex);
		if (ret)
		{
			printf("pthread_mutex_unlock failed %d\n", ret);
		}
	}
#endif
#endif
}

void SQLite3DB::ReleaseLockFromDb()
{
#ifndef WIN32
#ifndef _NVR2820_
	m_bDbLocked = false;
#endif
#endif
}

void SQLite3DB::Backup(const char *TargetBackupDBFileName)
{
#if SQLITE_VERSION_NUMBER >= 3006011
    CheckDB();

    sqlite3* pDBBake;
    sqlite3_backup* pBackup;
    int rc;
    rc = sqlite3_open(TargetBackupDBFileName, &pDBBake);
    if (rc != SQLITE_OK)
    {
        const char *szError = sqlite3_errmsg(pDBBake);
        sqlite3_close(pDBBake);
        throw SQLite3Exception(rc, szError, DONT_DELETE_MSG);
    }

    pBackup = sqlite3_backup_init(pDBBake, "main", m_pDB, "main");
    if (pBackup == 0)
    {
        const char* szError = sqlite3_errmsg(pDBBake);
        sqlite3_close(pDBBake);
        throw SQLite3Exception(rc, szError, DONT_DELETE_MSG);
    }

    do
    {
        rc = sqlite3_backup_step(pBackup, BACKUP_PAGECOUNT);
        if (rc == SQLITE_BUSY || rc == SQLITE_LOCKED)
        {
          sqlite3_sleep(250);
        }
    }
    while (rc == SQLITE_OK || rc == SQLITE_BUSY || rc == SQLITE_LOCKED);

    sqlite3_backup_finish(pBackup);
    if (rc == SQLITE_DONE)
    {
        sqlite3_close(pDBBake);
    }
    else
    {
        const char* szError = sqlite3_errmsg(pDBBake);
        sqlite3_close(pDBBake);
        throw SQLite3Exception(rc, szError, DONT_DELETE_MSG);
    }

#else

    throw SQLite3Exception(SQLITE_WRAPPER_ERROR, "Backup not supported in this SQLite Version", DONT_DELETE_MSG);

#endif
}
